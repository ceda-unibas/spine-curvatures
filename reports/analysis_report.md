# Analysis report: RyR1 mutations and spine curvatures

Rodrigo C. G. Pena, Center for Data Analytics (CeDA), University of Basel

## The project

Project codename `spine-curvatures` is a collaboration between [CeDA][ceda] and the [Neuromuscular Research Laboratory][nrl] (Sinnreich/Treves) of the Department of Biomedicine at the University of Basel.

## Scientific background

The guiding question in this project is whether reduction of [RyR1](https://en.wikipedia.org/wiki/Ryanodine_receptor_1) expression in muscle spindles (in mice) leads to skeletal abnormalities, in particular aberrations in spine column curvature.

Towards that goal, two animal models, paired with their wild-type (WT) littermates, are studied:

1. `dHT`: mice carrying the compound heterozygous RyR1 mutations `RyR1p.Q1970fsX16+p.A4329D`. These recessive mutations are isogenic to those present in a severely affected child.
2. `Exon36`: mice carrying the `RyR1p.Q1970fsX16` mutation. This leads to the expression of a single WT allele.

## Main findings

The transgenic mice seem to manifest larger curvatures in general compared to their WT littermates, but the effect is only really evident in the segments between vertebras 13 and 23 of the spine column. There, both `dHT` and `Exon36` mice seem to display noticeable kyphosis, but the effect is stronger and more consistent in `dHT` mice.

## Reproducibility

The data used in this analysis consists of hand-annotated point clouds representing mouse spine columns that were imaged via X-ray microtomography. The dataset, along with further details on the data gathering process, can be found on 

> Cerqueira Gonzalez Pena, R., & Ruiz, A. (2024). 3D spinal column point clouds of mice with RyR1 mutations and their wild-type littermates (1.2.0) [Data set]. Zenodo. https://doi.org/10.5281/zenodo.12721977

Code to load, display and process this dataset, as well as reproduce this analysis is available on 

> https://gitlab.com/ceda-unibas/spine-curvatures

<div style="page-break-after: always;"></div>

## Methods

We first fit cubic splines to the 3D point clouds of each mouse in the dataset. These smooth curves are contrained to be flat (zero curvature) at its extremes, corresponding to the points that would lead to the tail and head connections. We do this to focus on the inner part of the annotated spine columns and avoid spurious curvature at the extremes, where we have no infomation how the spine column continues (if it even does so).

The cubic spline fit leads to a parametrization of the type $\gamma = \gamma(t) = (\gamma_x(t), \gamma_y(t), \gamma_z(t))$ for each spine column, where $t$ is a traversal parameter in the interval $[0, 1]$ such that $\gamma(0)$ is the point at the beginning of the parametrized smooth curve and $\gamma(1)$ is the point in the end. When traversing the spine column when going from $t=0$ to $t=1$, one visits the vertebras in order: first vertebras 1, 2, 3, ..., then finally 23, 24, and so on.

We can compute the *curvature*, $\kappa(t)$, of a parametrized curve $\gamma(t)$, at each value of $t$, using the standard formula
$$
\kappa(t) = \frac{\|\gamma^{\prime}(t) \times \gamma^{\prime\prime}(t)\|}{\|\gamma^{\prime}(t)\|^3},
$$
where $\gamma^\prime$ and $\gamma^{\prime\prime}$ are the velocity and acceleration curves of $\gamma$ with respect to $t$ and $\|\cdot\|$ is the usual Euclidean norm.

The *radius of curvature* of $\gamma(t)$ at each $t \in [0,1]$ is given by the reciprocal of the curvature:
$$
r(t) := \frac{1}{\kappa(t)}.
$$
We compare populations of mice (`dHT` vs. `WT` and `Exon36` vs. `WT`) via summaries of the curvatures of the smooth curves fitted to their spine column point clouds. These summaries are:

- *Maximum curvature* ($mm^{-1}$): $\underset{t \in [0,1]}{\max} \enspace \kappa(t)$
- *Minimum radius of curvature* ($mm$): if $t^\star$ is the point of maximum curvature, then the minimum radius of curvature is $1/\kappa(t^\star)$. Intuitively, it is the radius of the largest sphere that is tangent to $\gamma(t^\star)$.
- *Average curvature* ($mm^{-1}$): take uniformly-spaced samples $t_1, t_2, ..., t_n$ from $[0,1]$ and compute $\frac{1}{n} \sum_{i=1}^{n} \kappa(t_i)$

Those summaries are computed for the spine columns as a whole, but also per segment, using the vertebra bounds $\text{Segment}_1 = (1, 13)$, $\text{Segment}_2 = (13, 23)$. That is, $\text{Segment}_1$ is the region from vertebras 1 to 13 in the spine column and $\text{Segment}_2$ is the region from vertebras 13 to 23. We use these segments to try to separate lordosis from kyphosis effects.

Once the curvature information of each spine column of each of the groups of interest is summarized by a number, we can test for the hypothesis that the numbers do not come from the same distribution using a permutation test[^good2013]. The test statistic is the difference of means between the two groups. The tests are one-sided:

- For maximum and average curvatures, the alternative to the null hypothesis is that the test statistic is *greater* on the transgenic group.
- For minimum radius of curvature, the alternative is that the test statistic is *lesser* on the transgenic group.

## Results in detail

### `dHT` vs. `WT`

#### How curvature changes as we traverse the spine column

Figure 1 displays the curvature at uniformly-spaced points of the smooth spine curves of each mouse in the `dHT` and the `WT` groups. As one traverses the spine columns, there are two regions of relatively large curvature, interspaced by flatter regions. There seems to be a slight preference overall to larger curvatures in the `dHT` group, but nothing definitive.

![dHT_curvature_traversal](thumbnails/dHT_curvature_traversal.png)

Figure 1. Pointwise curvature of the smooth spine curves for dHT (red) and WT (blue) mice.

In Figure 2 we see the reciprocal information: the minimum radii of curvature as we traverse the spines. Once again, when considering the whole spine, there is a slight preference for smaller radii of curvature in the `dHT` group.

![dHT_radius_traversal](thumbnails/dHT_radius_traversal.png)

Figure 2. Pointwise radius of curvature of the smooth spine curves for dHT (red) and WT (blue) mice. Radii of curvature larger than 15mm (flatter regions of the spine) are masked out for better visibility.

#### Differences in the curvature summaries (whole spine)

Maybe extremes (such as the maximum curvature of the minimum radius of curvature), or the average curvature per mouse spine are more distinguishable between groups than the distribution of curvatures across the whole spine. That's what we test here.

Figure 3 shows boxplots of the distributions of the three curvature summaries we use. Each boxplot represents 5 samples, corrsponding to the 5 mice in each group. The differences between groups seem stronger when comparing the curvature summaries.

![dHT_boxplots](thumbnails/dHT_boxplots.png)

Figure 3. Boxplots of the three curvature summaries used in the analysis. Each boxplot represents the information from the 5 mice in the respective group.

To test how likely it would be to observe these differences by chance if the summary values came from the same distribution, we perform a permutation test. Table 1 displays the results of this testing. The p-values are all quite small, but, at a significance level of 5%, only the average curvature would pass the test.

| Summary type                       | Mean difference (test statistic) | p-value |
| ---------------------------------- | -------------------------------- | ------- |
| Maximum curvature ($mm^{-1}$)      | -0.11                            | 0.087   |
| Average curvature ($mm^{-1}$)      | -0.04                            | 0.024   |
| Minimum radius of curvature ($mm$) | 1.50                             | 0.056   |

Table 1. Hypothesis testing results for `dHT` vs. `WT` (whole spine column)

#### Differences in the curvature summaries (per segment)

What about if we restrict the summaries to sections of the spines? This way we can differenciate between excess curvature in the upper part of the spine column (kyphosis) or the in the lower part (lordosis). Figure 4 shows boxplots of the minimum radius of curvature per segment of the spine column, for both `dHT` and `WT` mice. The difference is really strong in the upper spine, indicating noticeable kyphosis in `dHT` mice.

![dHT_boxplots_per_segment](thumbnails/dHT_boxplots_per_segment.png)

Figure 4. Boxplots of the minimum radius of curvature per group, per spine column segment. $\text{Segment}_1$ corresponds to vertebras 1 to 13 (lower spine column) and $\text{Segment}_2$ to vertebras from 13 to 23 (upper spine column). Each boxplot represents the information from the 5 mice in the respective group.

Table 2 shows the results of testing, via permutation test, if the differences we see in minimum radius of curvature per segment could be expected by chance. On $\text{Segment}_2$ (upper spine), the test passes even at a significance level of 1%, mice in `dHT` group having, on average, minimum radii of curvature 2.24 $mm$ smaller than their `WT` counterparts.

| Summary type                       | Mean difference (test statistic) | p-value | Segment |
| ---------------------------------- | -------------------------------- | ------- | ------- |
| Minimum radius of curvature ($mm$) | 2.58                             | 0.063   | 1       |
| Minimum radius of curvature ($mm$) | 2.24                             | 0.008   | 2       |

Table 2. Hypothesis testing results for `dHT` vs. `WT` (per segment)

### `Exon36` vs. `WT`

#### How curvature changes as we traverse the spine column

Now we pass to the other transgenic group in the dataset, `Exon36`. Figures 5 and 6 show the pointwise curvatures and radius of curvatures of each mouse in the `Exon36` group, as well as of their `WT` littermates. These curves seem visually more uniform across groups than they were in the `dHT` vs. `WT` plots.

![Exon36_curvature_traversal](thumbnails/Exon36_curvature_traversal.png)

Figure 5. Pointwise curvature of the smooth spine curves for `Exon36` (red) and `WT` (blue) mice.

![Exon36_radius_traversal](thumbnails/Exon36_radius_traversal.png)

Figure 6. Pointwise radius of curvature of the smooth spine curves for `Exon36` (red) and `WT` (blue) mice. Radii of curvature larger than 15mm (flatter regions of the spine) are masked out for better visibility.

#### Differences in the curvature summaries (whole spine)

Looking at curvature summaries now, Figure 7 shows boxplots of their distributions for the `Exon36` vs. `WT` comparison.

![Exon36_boxplots](thumbnails/Exon36_boxplots.png)

Figure 7. Boxplots of the three curvature summaries used in the analysis. Each boxplot represents the information from the 5 mice in the respective group.

And Table 3 sums up the results of the permutation test on each of these three summaries. We see a similar situation as we had seen before in the `dHT` vs. `WT` comparison. All p-values are small and even the test statistics are similarly-valued.

| Summary type                       | Mean difference (test statistic) | p-value |
| ---------------------------------- | -------------------------------- | ------- |
| Maximum curvature ($mm^{-1}$)      | -0.10                            | 0.028   |
| Average curvature ($mm^{-1}$)      | -0.01                            | 0.040   |
| Minimum radius of curvature ($mm$) | 1.67                             | 0.028   |

Table 3. Hypothesis testing results for `Exon36` vs. `WT` (whole spine column)

#### Differences in the curvature summaries (per segment)

Focusing now in the lower spine and upper spine segments, Figure 8 shows boxplots of the minimum radius of curvature for the `Exon36` vs. `WT` comparison.

![Exon36_boxplots_per_segment](thumbnails/Exon36_boxplots_per_segment.png)

Figure 8. Boxplots of the minimum radius of curvature per group, per spine column segment. $\text{Segment}_1$ corresponds to vertebras 1 to 13 (lower spine column) and $\text{Segment}_2$ to vertebras from 13 to 23 (upper spine column). Each boxplot represents the information from the 5 mice in the respective group.

Finally, Table 4 summarizes the results of the permutation tests on the difference of mean minimum radius of curvature per segment. Interestingly, the test on  $\text{Segment}_2$ passes at significance level 5% for the `Exon36` vs. `WT` case as well. This would indicate noticeable kyphosis on the `Exon36` group, albeit with a smaller mean difference in minimum radius of curvature (1.21 $mm$) than in the `dHT` vs. `WT` setting. In the lower spine column, the `Exon36` and `WT` groups are essentially indistinguishable.

| Summary type                       | Mean difference (test statistic) | p-value | Segment |
| ---------------------------------- | -------------------------------- | ------- | ------- |
| Minimum radius of curvature ($mm$) | -0.03                            | 0.524   | 1       |
| Minimum radius of curvature ($mm$) | 1.21                             | 0.024   | 2       |

Table 4. Hypothesis testing results for `Exon36` vs. `WT` (per segment)

## Discussion

We studied here some of the effects that reduced RyR1 expression can have in the development of spine column abnormalities, focusing on descriptions of curvature. Although the tests seem to indicate throughout the manifestation of larger curvatures in the transgenic groups, the differences between the transgenic and wild-type littermates is only really strong if we restrict the analysis to the spine column segment between vertebras 13 and 23 (upper spine). There, the difference is clear between `dHT` and `WT` mice, with the latter having, on average, minimum radii of curvature that are 2.24mm smaller. The difference in the upper spine for the `Exon36` vs. `WT` case is also noticeable, but the average difference in minimum radius of curvature is smaller (1.21mm) and the `Exon36` group shows more intra-mice variability. 

Potential criticism of this work may touch:

1. The smooth curve fit that we use as a proxy for the actual spine column. There is always some uncertainty as to whether we fit curves that model well the behavior of the spine's shape in 3D, e.g., due to errors in the dataset annotations. However, interested parties can interactively explore these curves in 3D to visually assess good behavior at https://gitlab.com/ceda-unibas/spine-curvatures
2. The small sample size. Each group (transgenic or otherwise) is only represented by 5 individuals.



[ceda]: https://ceda.unibas.ch/
[nrl]: https://biomedizin.unibas.ch/en/research/research-groups/sinnreich/-treves-lab/

[^good2013]: Good, P. (2013). *Permutation tests: a practical guide to resampling methods for testing hypotheses*. Springer Science & Business Media.