# spine-curvatures

[![license][license-badge]][license]

Studying 3D spinal column point clouds of mice with [RyR1](https://en.wikipedia.org/wiki/Ryanodine_receptor_1) mutations and their wild-type littermates

![example_spine](assets/example_spine.png)

## Project status

Ongoing. You can read the latest analysis report [here](./reports/analysis_report.md).

## Installation

### In its own environment

First, clone this repository. Then, create a new conda environment for this project by running the following on your Terminal:

```sh
conda env create -f environment.yml
```

Activate the new environment with `conda activate spine-curvatures` (or `source activate spine-curvatures`, depending on your system) and install the necessary dependencies with

```sh
make install
```

Alternatively, you may explicitly run `pip3 install .`

### In an existing environment

If you want to install the `spinecurvatures` package in the environment of your choice, without having to clone the repository, you can also do so by running

```sh
pip3 install https://gitlab.com/ceda-unibas/spine-curvatures/-/archive/main/sleep-brain-atlas-main.tar.gz
```

## Usage

The notebooks in the `notebooks` directory provide contain case studies analyzing spine curvature data. See the [`README.md`](notebooks/README.md) file in that directory for more information, such as further setup instructions.

As a quick example, if you get a copy of the [project's data from Zenodo](https://zenodo.org/doi/10.5281/zenodo.12721977), you can replicate the 3D spine plot illustrating this README by running the following commands (replacing the data path with the actual one in your system):

```python
import os

from spinecurvatures import dataset, plot

data_path = os.path.join("path_to_dataset", "microtomography_data_dHT_mice.xlsx")
spine_dataset = dataset.SpineDataset(path=data_path)
example_spine = spine_dataset.spines[0]
curves = example_spine.fit_curves()

fig = plot.plot_spine(
    example_spine,
    curves=curves,
    colors=example_spine.coords["vertebra"],
    backend="plotly",
)
fig.show()
```

## Contributing

See [CONTRIBUTING][contributing].

## Authors and acknowledgment

This project is a collaboration between [CeDA][ceda] and the [Neuromuscular Research Laboratory][nrl] (Sinnreich/Treves) of the Department of Biomedicine at the University of Basel.

## License

This software is released under the [BSD 3-Clause License][license].

[ceda]: https://ceda.unibas.ch/
[contributing]: CONTRIBUTING.md
[nrl]: https://biomedizin.unibas.ch/en/research/research-groups/sinnreich/-treves-lab/
[license]: LICENSE
[license-badge]: https://img.shields.io/badge/license-BSD-green
