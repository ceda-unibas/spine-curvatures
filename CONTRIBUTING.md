# Contributing

Contributions are welcome and appreciated. The development of this package takes place on [GitLab][spine-curvatures]. Issues, bugs, and feature requests should be reported there. Code and documentation can be improved by submitting a pull request. Please add documentation for any new code snippet. You can improve or add functionality in the repository by forking from main, developing your code, and then creating a pull request.

[spine-curvatures]: https://gitlab.com/ceda-unibas/spine-curvatures
