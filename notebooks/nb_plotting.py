import matplotlib.pyplot as plt
import nb_utils
import numpy as np

from spinecurvatures import dataset, plot, utils


def _get_subplot_dimensions(spine_dataset):
    # Subplot dimension are: number of groups (rows) x number of spines (cols)
    group_keys = list(spine_dataset.groups.keys())
    n_groups = len(group_keys)
    n_spines = 0
    for group in group_keys:
        n_spines = max(n_spines, len(spine_dataset.get_spines_from_group(group)))
    return n_groups, n_spines


def plot_all_spines(
    spine_dataset: dataset.SpineDataset, curvature_info, with_longitudinal_cuts=False
) -> tuple:
    """Plot all spines in the dataset

    Parameters
    ----------
    spine_dataset : `SpineDataset`
        Dataset containing all spines to be plotted
    curvature_info : dict
        Dictionary containing curvature information for all spines. See output of
        `nb_utils.compute_curvature_summaries
    with_longitudinal_cuts : bool, optional
        Whether to plot longitudinal cuts of the spines, by default False

    Returns
    -------
    `matplotlib.figure.Figure`, `matplotlib.axes.Axes`
        Figure and axis objects containing the plot
    """
    fig = plt.figure(figsize=(30, 12))
    n_groups, n_spines = _get_subplot_dimensions(spine_dataset)

    # Loop through all spines and plot them in the subplot grid. One row per group.
    i = 0
    group_keys = list(spine_dataset.groups.keys())
    for group in group_keys:
        for j, spine in enumerate(spine_dataset.get_spines_from_group(group)):
            # Plot spine
            ax = fig.add_subplot(n_groups, n_spines, j + i + 1, projection="3d")
            curves = curvature_info[group]["curves"][j]
            if with_longitudinal_cuts:
                constraint_plane = utils.get_curve_extremes_vectors(curves)
                coords = utils.get_curve_coords(curves)
                origin = constraint_plane[1] + 0.5 * constraint_plane[0]
                ax = plot.plot_spine(spine, curves, ax=ax, origin=origin)
                ax = plot.plot_longitudinal_cut(
                    spine, coords=coords, ax=ax, constraint_plane=constraint_plane
                )
                title = "[{0}]".format(spine.mouse_id)
            else:
                ax = plot.plot_spine(spine, curves, ax=ax)
                title = "[{0}] \n max. curv. = {1:.2f} \n avg. curv. = {2:.2f}".format(
                    spine.mouse_id,
                    curvature_info[group]["max_curvatures"][j],
                    curvature_info[group]["avg_curvatures"][j],
                )
            ax.set_title(title)
            ax.get_legend().remove()
        i += n_spines

    # Set legend back up for the last spine
    ax.legend(fontsize=14)

    fig.suptitle(
        "{} (first row) vs. {} (second row)".format(*tuple(group_keys)),
        fontsize=28,
    )

    return fig


def plot_permutation_distribution(res, ax=None, bins=21) -> plt.Axes:
    """Plot permutation distribution

    Parameters
    ----------
    res : `PermutationTestResult`
        Permutation test result, as returned by `scipy.stats.permutation_test`
    ax : `matplotlib.Axis`, optional
        Axis onto which to make the plot. If None, a new figure/axis will be
        created, by default None
    bins : int, optional
        Number of bins in the permutation distribution histogram, by default 11

    Returns
    -------
    `matplotlib.Axis`
        Axis onto which the plot was made
    """
    # Define color palette
    palette = plot.UnibasColors()

    # Parse axis
    if ax is None:
        _, ax = plt.subplots(1, 1, figsize=(4.5, 3))

    # Plot histogram os test statistics across different permutations
    ax.hist(res.null_distribution, bins=bins, color=palette.anthrazit_hell)

    # Overlay test statistic of the original permutation as a vertical line
    ax.axvline(x=res.statistic, color=palette.rot, label="Original statistic")

    # Set titles and labels
    ax.set_title("Permutation distribution of test statistic")
    ax.set_xlabel("Value of Statistic")
    ax.set_ylabel("Frequency")
    ax.legend()

    return ax


def plot_cumulative_distribution(
    samples, log_density=None, xrange=None, ax=None
) -> plt.Axes:
    """Plot cumulative distribution

    Cumulative histogram overlaid with a smoothed cumulative distribution
    obtained from kernel density estimation.

    Parameters
    ----------
    samples : list-like
        List of samples from which to compute the histogram
    log_density : callable, optional
        Log-density function with the same interface as
        `sklearn.neighbors.KernelDensity.score_samples`. It will be
        automatically computed if not provided
    xrange : tuple, optional
        Minimum and maximum values to display on the x-axis of the histogram,
        by default `(0, numpy.max(samples))`
    ax : `matplotlib.axes.Axes`, optional
        The axis onto which make the plot. A new figure will be created by
        default

    Returns
    -------
    `matplotlib.axes.Axes`
        Axis onto which the plot was made
    """
    # Parse range of the x-axis
    xrange = (0, np.max(samples)) if xrange is None else xrange

    # Sample lots of points from the log density function for plotting
    num_pts = 1000
    if log_density is None:
        log_density = nb_utils.estimate_log_density(samples)
    locations = np.linspace(xrange[0], xrange[1], num=num_pts)
    density = np.exp(log_density(locations.reshape(-1, 1)))
    cum_distr = density.cumsum()
    cum_distr /= cum_distr[-1]

    fontsize = 14
    if ax is None:
        _, ax = plt.subplots(1, 1, figsize=(9, 6))
        ax.set_title("Scoliosis distance cumulative distribution", fontsize=fontsize)
        ax.set_xlabel("Distance", fontsize=fontsize)
        ax.set_ylabel("Likelihood", fontsize=fontsize)

    palette = plot.UnibasColors()

    # Plot cumulative histogram
    num_bins = 50
    ax.hist(
        samples,
        bins=num_bins,
        range=xrange,
        color=palette.anthrazit_hell,
        density=True,
        cumulative=True,
    )

    # Plot smoothed cumulative distribution obtained from the kernel density
    # estimation
    ax.plot(
        locations,
        cum_distr,
        color=palette.mint,
        label="Kernel density estimation",
        linewidth=3,
    )

    ax.legend(fontsize=fontsize)

    return ax


def plot_scoliosis_summaries(scoliosis_info, xrange=None):
    """Plot scoliosis distance histograms

    Parameters
    ----------
    scoliosis_info : dict
        Dictionary containing scoliosis information for all spines. See output of
        `nb_utils.compute_scoliosis_summaries`
    xrange : tuple, optional
        Minimum and maximum values to display on the x-axis of the histogram,
        by default None

    Returns
    -------
    `matplotlib.figure.Figure`
        Figure object containing the plot
    """
    fig = plt.figure(figsize=(30, 12))

    # Set a fixed range for axis of all histograms
    yrange = (0, 1.1)

    # Get dimensions of the subplot grid
    group_keys = list(scoliosis_info.keys())
    n_groups = len(group_keys)
    n_spines = 0
    for group in group_keys:
        n_spines = max(n_spines, len(scoliosis_info[group]))

    # Loop through all groups and plot scoliosis distance histograms
    i = 0
    for group in group_keys:
        for j, mouse_id in enumerate(scoliosis_info[group].keys()):
            # Gather summaries for the current spine
            scoliosis_distances = scoliosis_info[group][mouse_id][
                "Scoliosis distance (mm)"
            ]
            log_density = scoliosis_info[group][mouse_id]["Log-density"]
            max_scoliosis = scoliosis_info[group][mouse_id]["Maximum scoliosis"]

            # Plot cumulative distribution of scoliosis distances
            ax = fig.add_subplot(n_groups, n_spines, j + i + 1)
            ax = plot_cumulative_distribution(
                scoliosis_distances, log_density=log_density, xrange=xrange, ax=ax
            )

            # Mark the maximum as a vertical line
            palette = plot.UnibasColors()
            ax.axvline(
                x=max_scoliosis,
                color=palette.rot,
                label="Max. scoliosis distance = {:.1f}".format(max_scoliosis),
                linewidth=3,
                ls="--",
            )

            ax.set_title(mouse_id)
            ax.set_ylim(yrange)
            ax.legend(fontsize=12)
        i += n_spines

    return fig
