{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Comparing spine curvatures: wild-type versus transgenic\n",
    "\n",
    "This notebooks looks at the differences in spine curvatures between either dHT or Exon36 mice and their wild-type (WT) littermates.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "import warnings\n",
    "\n",
    "import matplotlib.pyplot as plt\n",
    "import nb_plotting\n",
    "import nb_utils\n",
    "import numpy as np\n",
    "import plotly.express as px\n",
    "from scipy.stats import permutation_test\n",
    "\n",
    "from spinecurvatures import dataset, plot\n",
    "\n",
    "with warnings.catch_warnings():\n",
    "    warnings.simplefilter(\"ignore\")\n",
    "    import pandas as pd\n",
    "\n",
    "%reload_ext autoreload\n",
    "%autoreload 2\n",
    "\n",
    "plt.rcParams[\"font.family\"] = \"serif\"\n",
    "plt.rcParams[\"font.serif\"] = [\"Times New Roman\"]\n",
    "\n",
    "# CHOOSE HERE WHICH SUB-DATASET TO ANALYZE\n",
    "dataset_acronym = \"dHT\"\n",
    "# dataset_acronym = \"Exon36\"\n",
    "\n",
    "# Set data path\n",
    "SPINE_CURVATURES_DATA = os.environ.get(\"SPINE_CURVATURES_DATA\")\n",
    "DATA_PATH = os.path.join(\n",
    "    SPINE_CURVATURES_DATA, \"microtomography_data_{}_mice.xlsx\".format(dataset_acronym)\n",
    ")\n",
    "# Create directory for saving figures\n",
    "FIGURES_DIR = os.path.join(SPINE_CURVATURES_DATA, \"figures\")\n",
    "os.makedirs(FIGURES_DIR, exist_ok=True)\n",
    "\n",
    "# Create directory for saving results\n",
    "RESULTS_DIR = os.path.join(SPINE_CURVATURES_DATA, \"results\")\n",
    "os.makedirs(RESULTS_DIR, exist_ok=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Load the dataset\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "spine_dataset = dataset.SpineDataset(path=DATA_PATH)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Summaries of spine curvatures\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here's a visual summary of the smooth curves fitted to the spines of each mice in Group 1 (WT, first row) and Group 2 (transgenic, second row). Under each mouse ID, we plot the maximum and average curvature of the respective smooth fitted spine curve.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "curvature_info = nb_utils.compute_curvature_summaries(spine_dataset)\n",
    "fig = nb_plotting.plot_all_spines(spine_dataset, curvature_info)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It may not be easy to see the all the spines in the static plot above, so you can select some mouse ID and plot its spine in 3D for a better view.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mouse_id = 36420 if dataset_acronym == \"dHT\" else 38635\n",
    "example_spine = spine_dataset.get_spines_from_id(mouse_id)\n",
    "fig = plot.plot_spine(\n",
    "    example_spine,\n",
    "    curves=example_spine.curves,\n",
    "    colors=example_spine.coords[\"vertebra\"],\n",
    "    backend=\"plotly\",\n",
    ")\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can have the curvature information at each point of the smooth curves, so rather than looking at point summaries, we could also plot how the curvature changes as we traverse the spine.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "curvature_df = nb_utils.compute_pointwise_curvature(spine_dataset)\n",
    "curvature_df.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the plot below, we vary the traversal parameter of the spine curves from 0 to 1 and plot the corresponding curvature value on the y-axis. We do this for each mouse, overlaying the plots and coloring them by group.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "with warnings.catch_warnings():\n",
    "    # Ignore FutureWarning from Plotly\n",
    "    warnings.simplefilter(\"ignore\")\n",
    "    fig = px.scatter(\n",
    "        curvature_df,\n",
    "        x=\"Traversal parameter\",\n",
    "        y=\"Curvature (1/mm)\",\n",
    "        title=\"Curvature as one traverses the spine\",\n",
    "        color=\"Group\",\n",
    "        hover_data={\"Mouse ID\": True, \"Curvature (1/mm)\": True},\n",
    "        marginal_y=\"box\",\n",
    "    )\n",
    "# Save as HTML\n",
    "fig.write_html(\n",
    "    os.path.join(FIGURES_DIR, \"{}_curvature_traversal.html\".format(dataset_acronym))\n",
    ")\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For the reciprocal perspective, we can plot instead the radius of curvature (1/curvature) as we traverse the spine. This is shown below.\n",
    "\n",
    "_Remark_: due to the inverse operation, we limit the plot to not so large radii of curvature. You can play around with this threshold, but in general we are interested, in this study, in the curvier parts of the spine, not the flat ones.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pruning_threshold = 15  # mm\n",
    "pruned_df = curvature_df[curvature_df[\"Radius of curvature (mm)\"] < pruning_threshold]\n",
    "\n",
    "with warnings.catch_warnings():\n",
    "    # Ignore FutureWarning from Plotly\n",
    "    warnings.simplefilter(\"ignore\")\n",
    "    fig = px.scatter(\n",
    "        pruned_df,\n",
    "        x=\"Traversal parameter\",\n",
    "        y=\"Radius of curvature (mm)\",\n",
    "        title=\"Radius of curvature as one traverses the spine\",\n",
    "        color=\"Group\",\n",
    "        hover_data={\"Mouse ID\": True, \"Curvature (1/mm)\": True},\n",
    "        marginal_y=\"box\",\n",
    "    )\n",
    "# Save as HTML\n",
    "fig.write_html(\n",
    "    os.path.join(FIGURES_DIR, \"{}_radius_traversal.html\".format(dataset_acronym))\n",
    ")\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Testing for differences between groups\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We will use [permutation tests](https://en.wikipedia.org/wiki/Permutation_test) to compare curvature measurements coming from the two groups of mice. A permutation test first needs measurement samples from each group. These could be the maximum curvature of each spine (as we had above), but also average curvature, or whatever else we might be interested in probing. Second, a permutation test needs a statistic to compare the two groups. Under the null hypothesis, the measurements we take from each group should come from the same distribution, so we need a statistic that could be able to distinguish two different distributions. The difference of sample means is a good place to start.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def statistic(sample_1, sample_2, axis=0):\n",
    "    return np.mean(sample_1, axis=axis) - np.mean(sample_2, axis=axis)\n",
    "\n",
    "\n",
    "tests_df = pd.DataFrame(columns=[\"Summary type\", \"Mean difference\", \"p-value\"])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Difference in maximum curvature\n",
    "\n",
    "Let's gather tha maximum curvature of each spine in each group and box-plot them\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "group_keys = list(spine_dataset.groups.keys())\n",
    "group_1_max_curvatures = np.array(curvature_info[group_keys[0]][\"max_curvatures\"])\n",
    "group_2_max_curvatures = np.array(curvature_info[group_keys[1]][\"max_curvatures\"])\n",
    "\n",
    "_, ax = plt.subplots(figsize=(3, 3))\n",
    "columns = [list(group_1_max_curvatures), list(group_2_max_curvatures)]\n",
    "ax.boxplot(columns)\n",
    "plt.xticks(np.arange(len(columns)) + 1, group_keys)\n",
    "ax.set(xlabel=\"Group\", ylabel=\"Maximum Curvature (1/mm)\")\n",
    "plt.savefig(\n",
    "    os.path.join(\n",
    "        FIGURES_DIR,\n",
    "        \"{}_max_curvature_boxplot.png\".format(dataset_acronym),\n",
    "    ),\n",
    "    dpi=300,\n",
    "    bbox_inches=\"tight\",\n",
    ")\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The value of the test statistic here is:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"{:.2f}\".format(statistic(group_1_max_curvatures, group_2_max_curvatures)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To test if this difference is significant or could have happened simply by chance, we permute the measurements between groups, compute the statistic again and count how many times we reach a value at least as extreme as the one above. This is the essence of a permutation test, which will give us in the end a probability of obtaining a test statistic less than or equal to the observed value under the null hypothesis (p-value). We can then use this p-value to decide if the two samples come indeed from different populations or not.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "res = permutation_test(\n",
    "    (group_1_max_curvatures, group_2_max_curvatures),\n",
    "    statistic,\n",
    "    vectorized=True,\n",
    "    n_resamples=np.inf,\n",
    "    alternative=\"less\",\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The value of the statistic for the original set of measurements is available under `res.statistic`. As a sanity check, it coincides with the value that we computed manually before.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"{:.2f}\".format(res.statistic))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The p-value of the test is available under `res.pvalue`.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"p-value = {:.5f}\".format(res.pvalue))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us keep track of these values in the test dataframe.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "tests_df.loc[len(tests_df)] = [\"Max. Curvature (1/mm)\", res.statistic, res.pvalue]\n",
    "tests_df"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can also check the permutation distribution of the test statistic to have more insight.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ax = nb_plotting.plot_permutation_distribution(res)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Difference in average curvature\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can follow the same procedure to test for difference in average curvature.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "group_1_avg_curvatures = np.array(curvature_info[group_keys[0]][\"avg_curvatures\"])\n",
    "group_2_avg_curvatures = np.array(curvature_info[group_keys[1]][\"avg_curvatures\"])\n",
    "\n",
    "_, ax = plt.subplots(figsize=(3, 3))\n",
    "columns = [list(group_1_avg_curvatures), list(group_2_avg_curvatures)]\n",
    "ax.boxplot(columns)\n",
    "plt.xticks(np.arange(len(columns)) + 1, group_keys)\n",
    "ax.set(xlabel=\"Group\", ylabel=\"Average Curvature (1/mm)\")\n",
    "plt.savefig(\n",
    "    os.path.join(\n",
    "        FIGURES_DIR,\n",
    "        \"{}_avg_curvature_boxplot.png\".format(dataset_acronym),\n",
    "    ),\n",
    "    dpi=300,\n",
    "    bbox_inches=\"tight\",\n",
    ")\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "res = permutation_test(\n",
    "    (group_1_avg_curvatures, group_2_avg_curvatures),\n",
    "    statistic,\n",
    "    vectorized=True,\n",
    "    n_resamples=np.inf,\n",
    "    alternative=\"less\",\n",
    ")\n",
    "\n",
    "print(\"Statistic for the original measurements = {:.2f}\".format(res.statistic))\n",
    "print(\"p-value = {:.5f}\".format(res.pvalue))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "tests_df.loc[len(tests_df)] = [\"Avg. Curvature (1/mm)\", res.statistic, res.pvalue]\n",
    "tests_df"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ax = nb_plotting.plot_permutation_distribution(res)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Differences in minimum radius of curvature\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "group_1_min_radius_of_curvature = np.array(\n",
    "    curvature_info[group_keys[0]][\"min_radius_of_curvature\"]\n",
    ")\n",
    "group_2_min_radius_of_curvature = np.array(\n",
    "    curvature_info[group_keys[1]][\"min_radius_of_curvature\"]\n",
    ")\n",
    "\n",
    "_, ax = plt.subplots(figsize=(3, 3))\n",
    "columns = [list(group_1_min_radius_of_curvature), list(group_2_min_radius_of_curvature)]\n",
    "ax.boxplot(columns)\n",
    "plt.xticks(np.arange(len(columns)) + 1, group_keys)\n",
    "ax.set(xlabel=\"Group\", ylabel=\"Minimum Radius of Curvature (mm)\")\n",
    "plt.savefig(\n",
    "    os.path.join(\n",
    "        FIGURES_DIR,\n",
    "        \"{}_min_radius_of_curvature_boxplot.png\".format(dataset_acronym),\n",
    "    ),\n",
    "    dpi=300,\n",
    "    bbox_inches=\"tight\",\n",
    ")\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "res = permutation_test(\n",
    "    (group_1_min_radius_of_curvature, group_2_min_radius_of_curvature),\n",
    "    statistic,\n",
    "    vectorized=True,\n",
    "    n_resamples=np.inf,\n",
    "    alternative=\"greater\",\n",
    ")\n",
    "\n",
    "print(\"Statistic for the original measurements = {:.2f}\".format(res.statistic))\n",
    "print(\"p-value = {:.5f}\".format(res.pvalue))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "tests_df.loc[len(tests_df)] = [\n",
    "    \"Min. Radius of Curvature (mm)\",\n",
    "    res.statistic,\n",
    "    res.pvalue,\n",
    "]\n",
    "tests_df"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ax = nb_plotting.plot_permutation_distribution(res)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Save the results of the statistical tests\n",
    "tests_df.to_csv(\n",
    "    os.path.join(RESULTS_DIR, \"{}_statistical_tests.csv\".format(dataset_acronym)),\n",
    "    index=False,\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Differences by spine segment\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "All the summaries and tests so far were done globally, considering the whole of each of the mouse spines. However, researchers are interested in studying specific segments of the spine as well. There are two such segments in our context: **Segment 1** goes from vertebras 1 to 13, and **Segment 2** goes from vertebras 13 to 23. These segments are available under the `.segment_1` and `.segment_2` attributes of a `dataset.Spine` object.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"Segment 1 = {}\".format(example_spine.segment_1))\n",
    "print(\"Segment 2 = {}\".format(example_spine.segment_2))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can limit the fitted smooth curves to only the vertebras in a given segment by finding the traversal parameter limits that define the segment. This is possible with the method `find_segment_bounds()`.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "lower_s1, upper_s1 = example_spine.find_segment_bounds(example_spine.segment_1)\n",
    "print(\n",
    "    \"Bounds on the traversal parameter for Segment 1 = \"\n",
    "    + \"({0:.2f}, {1:.2f})\".format(lower_s1, upper_s1)\n",
    ")\n",
    "\n",
    "lower_s2, upper_s2 = example_spine.find_segment_bounds(example_spine.segment_2)\n",
    "print(\n",
    "    \"Bounds on the traversal parameter for Segment 2 = \"\n",
    "    + \"({0:.2f}, {1:.2f})\".format(lower_s2, upper_s2)\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Using these bounds, we can compute, say, the maximum curvature restricted only to a segment of the spine\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Compute max curvature for Segment 1\n",
    "max_curvature_seg1 = example_spine.max_curvature(bounds=(lower_s1, upper_s1)).fun\n",
    "\n",
    "# Compute max curvature for Segment 2\n",
    "max_curvature_seg2 = example_spine.max_curvature(bounds=(lower_s2, upper_s2)).fun"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And we can plot the smooth curves restricted to the two segments side by side to see that indeed they represent two different regions of the spine and their maximum curvatures are different.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = plt.figure(figsize=(16, 8))\n",
    "\n",
    "ax = fig.add_subplot(1, 2, 1, projection=\"3d\")\n",
    "plot.plot_spine(\n",
    "    example_spine,\n",
    "    curves=example_spine.curves,\n",
    "    s_vals=np.linspace(lower_s1, upper_s1, 2000),\n",
    "    colors=example_spine.coords[\"vertebra\"],\n",
    "    ax=ax,\n",
    ")\n",
    "ax.set_title(\n",
    "    \"Segment 1 [{}] \\n max. curvature = {:.2f} (1/mm)\".format(\n",
    "        example_spine.segment_1, max_curvature_seg1\n",
    "    ),\n",
    "    fontsize=16,\n",
    ")\n",
    "ax.get_legend().remove()\n",
    "\n",
    "ax = fig.add_subplot(1, 2, 2, projection=\"3d\")\n",
    "plot.plot_spine(\n",
    "    example_spine,\n",
    "    curves=example_spine.curves,\n",
    "    s_vals=np.linspace(lower_s2, upper_s2, 2000),\n",
    "    colors=example_spine.coords[\"vertebra\"],\n",
    "    ax=ax,\n",
    ")\n",
    "ax.set_title(\n",
    "    \"Segment 2 [{}] \\n max. curvature = {:.2f} (1/mm)\".format(\n",
    "        example_spine.segment_2, max_curvature_seg2\n",
    "    ),\n",
    "    fontsize=16,\n",
    ")\n",
    "\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Knowing that we can analyze individual segments of the spine, let us run through all the spines across the two groups in the dataset and compile curvature summaries for segments 1 and 2.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "curvature_info_per_segment = nb_utils.compute_curvature_summaries(\n",
    "    spine_dataset, segments=[(lower_s1, upper_s1), (lower_s2, upper_s2)]\n",
    ")\n",
    "nb_utils.print_curvature_info_per_segment(curvature_info_per_segment)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "With all these curvature summaries compiled, we may run permutation tests to test for curvature differences between groups, but now restricted to segments of the vertebral column.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "tests_per_segment_df = pd.DataFrame(\n",
    "    columns=[\"Summary type\", \"Mean difference\", \"p-value\", \"Segment\"]\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Differences in maximum curvature for Segment 1\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "segment = 1\n",
    "_, ax = plt.subplots(figsize=(3, 3))\n",
    "columns = [\n",
    "    list(curvature_info_per_segment[segment - 1][group_keys[0]][\"max_curvatures\"]),\n",
    "    list(curvature_info_per_segment[segment - 1][group_keys[1]][\"max_curvatures\"]),\n",
    "]\n",
    "ax.boxplot(columns)\n",
    "plt.xticks(np.arange(len(columns)) + 1, group_keys)\n",
    "ax.set(xlabel=\"Group\", ylabel=\"Maximum Curvature [Segment {}] (1/mm)\".format(segment))\n",
    "plt.savefig(\n",
    "    os.path.join(\n",
    "        FIGURES_DIR,\n",
    "        \"{}_max_curvature_segment_{}_boxplot.png\".format(dataset_acronym, segment),\n",
    "    ),\n",
    "    dpi=300,\n",
    "    bbox_inches=\"tight\",\n",
    ")\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "res = permutation_test(\n",
    "    (\n",
    "        curvature_info_per_segment[segment - 1][group_keys[0]][\"max_curvatures\"],\n",
    "        curvature_info_per_segment[segment - 1][group_keys[1]][\"max_curvatures\"],\n",
    "    ),\n",
    "    statistic,\n",
    "    vectorized=True,\n",
    "    n_resamples=np.inf,\n",
    "    alternative=\"less\",\n",
    ")\n",
    "\n",
    "print(\"Statistic for the original measurements = {:.2f}\".format(res.statistic))\n",
    "print(\"p-value = {:.5f}\".format(res.pvalue))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "tests_per_segment_df.loc[len(tests_per_segment_df)] = [\n",
    "    \"Max. Curvature (1/mm)\",\n",
    "    res.statistic,\n",
    "    res.pvalue,\n",
    "    segment,\n",
    "]\n",
    "tests_per_segment_df"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ax = nb_plotting.plot_permutation_distribution(res)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Differences in maximum curvature for Segment 2\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "segment = 2\n",
    "_, ax = plt.subplots(figsize=(3, 3))\n",
    "columns = [\n",
    "    list(curvature_info_per_segment[segment - 1][group_keys[0]][\"max_curvatures\"]),\n",
    "    list(curvature_info_per_segment[segment - 1][group_keys[1]][\"max_curvatures\"]),\n",
    "]\n",
    "ax.boxplot(columns)\n",
    "plt.xticks(np.arange(len(columns)) + 1, group_keys)\n",
    "ax.set(xlabel=\"Group\", ylabel=\"Maximum Curvature [Segment {}] (mm)\".format(segment))\n",
    "plt.savefig(\n",
    "    os.path.join(\n",
    "        FIGURES_DIR,\n",
    "        \"{}_max_curvature_segment_{}_boxplot.png\".format(dataset_acronym, segment),\n",
    "    ),\n",
    "    dpi=300,\n",
    "    bbox_inches=\"tight\",\n",
    ")\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "res = permutation_test(\n",
    "    (\n",
    "        curvature_info_per_segment[segment - 1][group_keys[0]][\"max_curvatures\"],\n",
    "        curvature_info_per_segment[segment - 1][group_keys[1]][\"max_curvatures\"],\n",
    "    ),\n",
    "    statistic,\n",
    "    vectorized=True,\n",
    "    n_resamples=np.inf,\n",
    "    alternative=\"less\",\n",
    ")\n",
    "\n",
    "print(\"Statistic for the original measurements = {:.2f}\".format(res.statistic))\n",
    "print(\"p-value = {:.5f}\".format(res.pvalue))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "tests_per_segment_df.loc[len(tests_per_segment_df)] = [\n",
    "    \"Max. Curvature (1/mm)\",\n",
    "    res.statistic,\n",
    "    res.pvalue,\n",
    "    segment,\n",
    "]\n",
    "tests_per_segment_df"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ax = nb_plotting.plot_permutation_distribution(res)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Differences in minimum radius of curvature for Segment 1\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "segment = 1\n",
    "_, ax = plt.subplots(figsize=(3, 3))\n",
    "columns = [\n",
    "    list(\n",
    "        curvature_info_per_segment[segment - 1][group_keys[0]][\n",
    "            \"min_radius_of_curvature\"\n",
    "        ]\n",
    "    ),\n",
    "    list(\n",
    "        curvature_info_per_segment[segment - 1][group_keys[1]][\n",
    "            \"min_radius_of_curvature\"\n",
    "        ]\n",
    "    ),\n",
    "]\n",
    "ax.boxplot(columns)\n",
    "plt.xticks(np.arange(len(columns)) + 1, group_keys)\n",
    "ax.set(\n",
    "    xlabel=\"Group\", ylabel=\"Min. Radius of Curvature [Segment {}] (mm)\".format(segment)\n",
    ")\n",
    "plt.savefig(\n",
    "    os.path.join(\n",
    "        FIGURES_DIR,\n",
    "        \"{}_min_radius_of_curvature_segment_{}_boxplot.png\".format(\n",
    "            dataset_acronym, segment\n",
    "        ),\n",
    "    ),\n",
    "    dpi=300,\n",
    "    bbox_inches=\"tight\",\n",
    ")\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "res = permutation_test(\n",
    "    (\n",
    "        curvature_info_per_segment[segment - 1][group_keys[0]][\n",
    "            \"min_radius_of_curvature\"\n",
    "        ],\n",
    "        curvature_info_per_segment[segment - 1][group_keys[1]][\n",
    "            \"min_radius_of_curvature\"\n",
    "        ],\n",
    "    ),\n",
    "    statistic,\n",
    "    vectorized=True,\n",
    "    n_resamples=np.inf,\n",
    "    alternative=\"greater\",\n",
    ")\n",
    "\n",
    "print(\"Statistic for the original measurements = {:.2f}\".format(res.statistic))\n",
    "print(\"p-value = {:.5f}\".format(res.pvalue))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "tests_per_segment_df.loc[len(tests_per_segment_df)] = [\n",
    "    \"Min. Radius of Curvature (mm)\",\n",
    "    res.statistic,\n",
    "    res.pvalue,\n",
    "    segment,\n",
    "]\n",
    "tests_per_segment_df"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ax = nb_plotting.plot_permutation_distribution(res)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Differences in minimum radius of curvature for Segment 2\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "segment = 2\n",
    "_, ax = plt.subplots(figsize=(3, 3))\n",
    "columns = [\n",
    "    list(\n",
    "        curvature_info_per_segment[segment - 1][group_keys[0]][\n",
    "            \"min_radius_of_curvature\"\n",
    "        ]\n",
    "    ),\n",
    "    list(\n",
    "        curvature_info_per_segment[segment - 1][group_keys[1]][\n",
    "            \"min_radius_of_curvature\"\n",
    "        ]\n",
    "    ),\n",
    "]\n",
    "ax.boxplot(columns)\n",
    "plt.xticks(np.arange(len(columns)) + 1, group_keys)\n",
    "ax.set(\n",
    "    xlabel=\"Group\", ylabel=\"Min. Radius of Curvature [Segment {}] (mm)\".format(segment)\n",
    ")\n",
    "plt.savefig(\n",
    "    os.path.join(\n",
    "        FIGURES_DIR,\n",
    "        \"{}_min_radius_of_curvature_segment_{}_boxplot.png\".format(\n",
    "            dataset_acronym, segment\n",
    "        ),\n",
    "    ),\n",
    "    dpi=300,\n",
    "    bbox_inches=\"tight\",\n",
    ")\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "res = permutation_test(\n",
    "    (\n",
    "        curvature_info_per_segment[segment - 1][group_keys[0]][\n",
    "            \"min_radius_of_curvature\"\n",
    "        ],\n",
    "        curvature_info_per_segment[segment - 1][group_keys[1]][\n",
    "            \"min_radius_of_curvature\"\n",
    "        ],\n",
    "    ),\n",
    "    statistic,\n",
    "    vectorized=True,\n",
    "    n_resamples=np.inf,\n",
    "    alternative=\"greater\",\n",
    ")\n",
    "\n",
    "print(\"Statistic for the original measurements = {:.2f}\".format(res.statistic))\n",
    "print(\"p-value = {:.5f}\".format(res.pvalue))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "tests_per_segment_df.loc[len(tests_per_segment_df)] = [\n",
    "    \"Min. Radius of Curvature (mm)\",\n",
    "    res.statistic,\n",
    "    res.pvalue,\n",
    "    segment,\n",
    "]\n",
    "tests_per_segment_df"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ax = nb_plotting.plot_permutation_distribution(res)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Save the results of the statistical tests\n",
    "tests_per_segment_df.to_csv(\n",
    "    os.path.join(\n",
    "        RESULTS_DIR, \"{}_statistical_tests_per_segment.csv\".format(dataset_acronym)\n",
    "    ),\n",
    "    index=False,\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.1"
  },
  "vscode": {
   "interpreter": {
    "hash": "dd854e12a6a0e7de6bed4f13c9025e8341a08bfd29d8845554d66694bcec92da"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
