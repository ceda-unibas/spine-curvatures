# README: Notebooks

This directory contains Jupyter notebooks that provide case studies analyzing spine curvature data. The notebooks are numbered in the suggested reading order.

## Setup

### Install notebook dependencies

On top of the usual installation instructions in the main README, you will need to install some extra packages to run the notebooks. You can do so with the following command:

```sh
pip install -r requirements.txt
```

### Get a copy of the data

This project's dataset is available on Zenodo: [![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.12721977.svg)](https://zenodo.org/doi/10.5281/zenodo.12721977)

The notebooks expect the data to be in the directory pointed to by the `SPINE_CURVATURES_DATA` environment variable. Set this variable up in your system (e.g., via your `.bashrc` or `.bash_profile` file on Unix systems) to point to the directory where you have the data stored. Alternatively, you can modify the `DATA_DIR` variable in each of the notebooks to point to the correct location.
