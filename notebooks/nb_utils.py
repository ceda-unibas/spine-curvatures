import warnings

import numpy as np
from sklearn.model_selection import GridSearchCV
from sklearn.neighbors import KernelDensity

from spinecurvatures import dataset, models, utils

with warnings.catch_warnings():
    warnings.simplefilter("ignore")
    import pandas as pd


def _get_curvature_info(spine, bounds=None):
    maximization_results = spine.max_curvature(bounds=models.parse_bounds(bounds))
    point_of_max_curvature = maximization_results.x
    min_radius_of_curvature = spine.radius_of_curvature(point_of_max_curvature)
    max_curvature = maximization_results.fun
    avg_curvature = spine.avg_curvature(bounds=models.parse_bounds(bounds))
    return max_curvature, avg_curvature, min_radius_of_curvature


def compute_curvature_summaries(spine_dataset: dataset.SpineDataset, segments=None):
    """Compute curvature summaries for all spines in the dataset

    Parameters
    ----------
    spine_dataset : `SpineDataset`
        Dataset containing all spines to be analyzed
    segments : list, optional
        List of bounds to be used for computing the curvature summaries, by default None. You can use this parameter to restrict the computation of summaries to specific segments of the spines. Each element in the list should be a tuple of the form (start, end), where start and end are arc lengths in the range [0, 1]. To convert vertebra indices to arc length, see `spinecurvatures.models.Spine.find_segment_bounds`

    Returns
    -------
    list or dict
        List of dictionaries containing curvature information for all spines in the dataset. The list has one element per segment. If only one segment was computed, a single dictionary is returned. The dictionary has the following structure:
        {
            group_name: {
                curves: list of `scipy.interpolate.BSpline` objects,
                max_curvatures: list of float,
                avg_curvatures: list of float,
                min_radius_of_curvature: list of float
            }
        }
    """
    curvature_info = []
    group_keys = list(spine_dataset.groups.keys())
    if segments is None:
        segments = [models.parse_bounds()]
    for i in range(len(segments)):
        curvature_info.append({})
        for group in group_keys:
            curvature_info[i][group] = {
                "curves": [],
                "max_curvatures": [],
                "avg_curvatures": [],
                "min_radius_of_curvature": [],
            }
            for j, spine in enumerate(spine_dataset.get_spines_from_group(group)):
                # Get curvature information
                max_curvature, avg_curvature, min_radius_of_curvature = (
                    _get_curvature_info(spine, bounds=segments[i])
                )
                curvature_info[i][group]["curves"].append(spine.curves)
                curvature_info[i][group]["max_curvatures"].append(max_curvature)
                curvature_info[i][group]["avg_curvatures"].append(avg_curvature)
                curvature_info[i][group]["min_radius_of_curvature"].append(
                    min_radius_of_curvature
                )
    # If only one segment was computed, return a single dictionary
    if len(segments) == 1:
        return curvature_info[0]
    return curvature_info


def print_curvature_info_per_segment(curvature_info_per_segment):
    """Shortcut to print curvature information per segment

    Parameters
    ----------
    curvature_info_per_segment : list
        List of dictionaries containing curvature information for all spines in the dataset. See output of `compute_curvature_summaries`
    """
    n_segments = len(curvature_info_per_segment)
    for i in range(n_segments):
        print("Segment {}:".format(i + 1))
        for group, info in curvature_info_per_segment[i].items():
            print("\t{}:".format(group))
            print(
                "\t\tmax. curvatures = ["
                + " ".join(["{:.2f}".format(val) for val in info["max_curvatures"]])
                + "]"
            )
            print(
                "\t\tmin. radius of curvature = ["
                + " ".join(
                    ["{:.2f}".format(val) for val in info["min_radius_of_curvature"]]
                )
                + "]"
            )


def compute_scoliosis_distances(
    spine_dataset: dataset.SpineDataset, curvature_info=None, bounds=None, n_points=1000
) -> pd.DataFrame:
    """Gather scoliosis distances for all spines in the dataset

    Parameters
    ----------
    spine_dataset : `SpineDataset`
        Dataset containing all spines to be analyzed
    curvature_info : dict, optional
        Dictionary containing curvature information for all spines in the dataset, by default None. See output of `compute_curvature_summaries`. If not provided, the smooth spine curves will be fitted computed from scratch.
    bounds : tuple, optional
        Tuple containing the start and end of the traversal parameter to be used for computing the scoliosis distances, by default None
    n_points : int, optional
        Number of uniformly-spaced points along the spines for which to compute the scoliosis distances, by default 1000

    Returns
    -------
    `pandas.DataFrame`
        Data frame containing the scoliosis distances for all spines in the dataset
    """
    # Set single list of traversal parameters for all spines
    bounds = models.parse_bounds(bounds)
    s = np.linspace(bounds[0], bounds[1], n_points)

    # Loop through all spines gathering distances from the curves to the
    # longitudinal planes
    scoliosis_df = pd.DataFrame()
    group_keys = list(spine_dataset.groups.keys())
    for group in group_keys:
        # Gather information within group
        for i, spine in enumerate(spine_dataset.get_spines_from_group(group)):
            # Get smooth curve coordinates
            if curvature_info is not None:
                curves = curvature_info[group]["curves"][i]
            else:
                curves = spine.curves
            curve_coords = utils.get_curve_coords(curves, s=s)

            # Get list of scoliosis distances coordinate
            scoliosis_distances = spine.get_scoliosis_distances(coords=curve_coords)

            # Append information to data frame
            scoliosis_df = pd.concat(
                [
                    scoliosis_df,
                    pd.DataFrame(
                        {
                            "Traversal parameter": s,
                            "Scoliosis distance (mm)": scoliosis_distances,
                            "Mouse ID": spine.mouse_id,
                            "Group": group,
                        }
                    ),
                ]
            )

    return scoliosis_df


def compute_scoliosis_summaries(scoliosis_df):
    """Compute scoliosis summaries for all spines in the dataset

    Parameters
    ----------
    scoliosis_df : `pandas.DataFrame`
        Data frame containing the scoliosis distances for all spines in the dataset

    Returns
    -------
    dict
        Dictionary containing scoliosis summaries for all spines in the dataset. The dictionary has the following structure:
        {
            group_name: {
                max_scoliosis: float,
                avg_scoliosis: float
            }
        }
    """
    scoliosis_summaries = {}
    grouping = scoliosis_df.groupby("Group")
    for group, group_df in grouping:
        scoliosis_summaries[group] = {}
        mouse_id_grouping = group_df.groupby("Mouse ID")
        for mouse_id, mouse_id_df in mouse_id_grouping:
            scoliosis_summaries[group][mouse_id] = {
                "Scoliosis distance (mm)": mouse_id_df[
                    "Scoliosis distance (mm)"
                ].values,
                "Maximum scoliosis": mouse_id_df["Scoliosis distance (mm)"].max(),
                "Log-density": estimate_log_density(
                    mouse_id_df["Scoliosis distance (mm)"]
                ),
            }
    return scoliosis_summaries


def estimate_log_density(samples, bandwidths=None, kernel="gaussian"):
    """Estimate log-density function from samples of a random variable

    Parameters
    ----------
    samples : array-like
        List of samples
    bandwidths : array-like, optional
        List of bandwidths to try, by default `numpy.np.arange(0.01, 0.5, 0.1)`. Among
        the kernel density estimators associated with these bandwidths, the one
        that maximizes the log-likelihood of the data will be picked
    kernel : str, optional
        Type of kernel to use in the estimation, by default 'gaussian'. See
        `sklearn.neighbors.KernelDensity`

    Returns
    -------
    callable
        Log-density function. Pass to it a list of numbers and it will return
        the estimated log-density values at these numbers. See
        `sklearn.neighbors.KernelDensity.score_samples`
    """
    # Parse samples
    samples = np.array(samples).reshape(-1, 1)

    # Parse bandwidth
    if bandwidths is None:
        bandwidths = np.arange(0.01, 0.5, 0.1)

    # Find best kernel density estimate within the bandwidth grid
    kde = KernelDensity(kernel=kernel)
    grid = GridSearchCV(kde, {"bandwidth": bandwidths})
    grid.fit(samples)

    # Return log density from the best estimate
    kde = grid.best_estimator_
    return kde.score_samples


def compute_pointwise_curvature(
    spine_dataset: dataset.SpineDataset, n_points=101
) -> pd.DataFrame:
    """Compute pointwise curvature and radius of curvature for all spines in the dataset

    Parameters
    ----------
    spine_dataset : `SpineDataset`
        Dataset containing all spines to be plotted
    n_points : int, optional
        Number of uniformly-spaced points along the spines for which to compute the curvature, by default 101

    Returns
    -------
    pandas.DataFrame
        Data frame containing the curvature information for all spines in the dataset. The data frame has the following columns:
        - 'Traversal parameter': traversal parameter along the spine
        - 'Curvature (1/mm)': curvature at the given traversal parameter
        - 'Mouse ID': ID of the mouse to which the spine belongs
        - 'Group': group to which the spine belongs
    """
    # Set single list of traversal parameters for all spines
    s = np.linspace(0, 1, n_points)

    # Loop through all spines and plot curvature vs. traversal parameter, overlaying all plots
    curvature_df = pd.DataFrame()
    group_keys = list(spine_dataset.groups.keys())
    for group in group_keys:
        # Gather information within group
        for i, spine in enumerate(spine_dataset.get_spines_from_group(group)):
            # Get list of curvatures per traversal parameter
            curvatures = [spine.curvature(val) for val in s]

            # Get list of radius of curvature per traversal parameter
            radius_of_curvatures = [1 / curvature for curvature in curvatures]

            # Append information to data frame
            new_df = pd.DataFrame(
                {
                    "Traversal parameter": s,
                    "Curvature (1/mm)": curvatures,
                    "Radius of curvature (mm)": radius_of_curvatures,
                    "Mouse ID": spine.mouse_id,
                    "Group": group,
                }
            )
            curvature_df = pd.concat([curvature_df, new_df])

    return curvature_df
