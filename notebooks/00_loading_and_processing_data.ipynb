{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Loading and processing spine data\n",
    "\n",
    "This notebook shows how to load and process the spine point cloud data available [here](https://zenodo.org/doi/10.5281/zenodo.12721977) with the `spinecurvatures` package\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "import warnings\n",
    "\n",
    "import matplotlib.pyplot as plt\n",
    "import nb_plotting\n",
    "import nb_utils\n",
    "import numpy as np\n",
    "import plotly.express as px\n",
    "\n",
    "from spinecurvatures import dataset, plot, utils\n",
    "\n",
    "with warnings.catch_warnings():\n",
    "    warnings.simplefilter(\"ignore\")\n",
    "    import pandas as pd\n",
    "\n",
    "%reload_ext autoreload\n",
    "%autoreload 2\n",
    "\n",
    "SPINE_CURVATURES_DATA = os.environ[\"SPINE_CURVATURES_DATA\"]\n",
    "dataset_acronym = \"dHT\"  # You can choose this dataset,\n",
    "# dataset_acronym = \"Exon36\"  # or this one. Just uncomment the one you want to use\n",
    "DATA_PATH = os.path.join(\n",
    "    SPINE_CURVATURES_DATA, \"microtomography_data_{}_mice.xlsx\".format(dataset_acronym)\n",
    ")\n",
    "\n",
    "palette = plot.UnibasColors()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# The dataset\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The class `dataset.SpineDataset` loads the `.xlsx` spine data and formats it into a convenient structure\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "spine_dataset = dataset.SpineDataset(path=DATA_PATH)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `group` attribute tells us which mice (represented by their ID) belong to which group\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "spine_dataset.groups"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The attribute `spines` contains a list of `dataset.Spine` objects, one for each mouse ID in the dataset.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Get first spine from the list\n",
    "number = 0\n",
    "example_spine = spine_dataset.spines[number]\n",
    "\n",
    "print(\"Spine {0} belongs to mouse {1}\".format(number, example_spine.mouse_id))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "One may also retrieve a spine by specifying mouse ID (here we use the first mouse ID, but you can use any other. See spine_dataset.groups for options)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "example_spine = spine_dataset.get_spines_from_id(example_spine.mouse_id)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A `dataset.Spine` object stores the coordinates of the spine's vertebra points under the attribute `coords`.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "example_spine.coords.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Fitting smooth curves\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A `model.Spine` can fit curves (smooth splines) to its vertebra coordinates via the `fit_curves()` method. This is automatically done when we create the Spines via `dataset.SpineDataset`. These curves are a tuple of three `scipy.interpolate._bsplines.BSpline` objects, one for each of the X, Y, and Z dimensions:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "curves = example_spine.curves\n",
    "curves"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The smooth fitted curves are parametrized by a scalar (traversal) parameter. This real number varies between 0 and 1, with 0 indicating the beginning of the beginning of the curve and 1 indicating its end. We may scatter the measured vertebra coordinates for each of the X, Y, and Z dimensions and overlay this with a plot of the corresponding smooth curve to assess the quality of the fit\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, axes = plt.subplots(1, 3, sharey=True, figsize=(27, 6))\n",
    "plot.plot_spine_slice(\"X\", example_spine, curves, ax=axes[0])\n",
    "axes[0].set_title(\"X\", fontsize=28)\n",
    "plot.plot_spine_slice(\"Y\", example_spine, curves, ax=axes[1])\n",
    "axes[1].set_title(\"Y\", fontsize=28)\n",
    "plot.plot_spine_slice(\"Z\", example_spine, curves, ax=axes[2])\n",
    "axes[2].set_title(\"Z\", fontsize=28)\n",
    "plt.tight_layout()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We may also scatter the full 3D (interactive) picture, interpreting the points on the three smooth curves as the coordinates of a 3D smooth curve fitting the mouse spine. This will give us a better global view on how and where the spine bends.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = plot.plot_spine(\n",
    "    example_spine,\n",
    "    curves=curves,\n",
    "    colors=example_spine.coords[\"vertebra\"],\n",
    "    backend=\"plotly\",\n",
    ")\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Some summaries: maximum and average curvatures\n",
    "\n",
    "We can get the curvature of the fitted smooth curve for each arc length parameter value using the `.curvature()` method of `dataset.Spine`. For that we, need to provide either the `curves` we computed before or the corresponding `accelerations` to save on a bit of computation\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"Curvature at the beginning: {:.2f} (1/mm)\".format(example_spine.curvature(0.0)))\n",
    "print(\n",
    "    \"Curvature at a 1/4 of the way: {:.2f} (1/mm)\".format(example_spine.curvature(0.25))\n",
    ")\n",
    "print(\"Curvature at the midpoint: {:.2f} (1/mm)\".format(example_spine.curvature(0.5)))\n",
    "print(\n",
    "    \"Curvature at 3/4 of the way: {:.2f} (1/mm)\".format(example_spine.curvature(0.75))\n",
    ")\n",
    "print(\"Curvature at the end: {:.2f} (1/mm)\".format(example_spine.curvature(1.0)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We also directly get the maximum curvature (and at which traversal value it occurs) via the `.max_curvature()` method, and the average curvature via `.avg_curvature()`.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "res = example_spine.max_curvature()\n",
    "print(\n",
    "    \"The curvature is maximized at {:.2f} traversal\".format(res.x)\n",
    "    + \" and its value is {:.2f} (1/mm)\".format(res.fun)\n",
    ")\n",
    "\n",
    "print(\"The average curvature is {:.2f} (1/mm)\".format(example_spine.avg_curvature()))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Scoliosis\n",
    "\n",
    "The plane where the largest variations in curvature happen should correspond to a longitudinal cut of the mouse's spine. Vertebra deviations from this longitudinal plane can be thought of as a proxy for scoliosis.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Finding the longitudinal cuts of the spines\n",
    "\n",
    "This is how we define the longitudinal cut of a spine:\n",
    "\n",
    "1. Compute the (normalized) vector pointing from one end of the spine to the other. This vector should lie in the longitudinal plane. This constraint forces the longitudinal cut to intersect the spine at its extremes, thus guaranteeing that deviations from the longitudinal plane at these points are zero.\n",
    "2. Project the spine point cloud onto the plane whose normal is the vector we just computed, and run principal component analysis (PCA) on the projected points. The second vector completing the basis of the longitudinal cut is then defined as the first of the principal components. Justifiably, this first principal component points at the direction of largest variation in the plane orthogonal to the vector connecting the two spine extremities. In passing, we note that the second principal component, which is orthogonal to the two vectors we just described, is precisely the normal vector of the longitudinal plane.\n",
    "\n",
    "In the cell below, we use samples from the smooth curve fitted to the spine (obtained via `utils.get_curve_coords()`) as the \"point cloud\" from which to compute the longitudinal plane vectors. The smooth fitted curve is an idealized, denoised version of the spine, which leads to a better estimate of what one would expect the longitudinal cut to look like. You may however choose another coordinate point cloud if you see fit. For instance, setting `example_coords` to `example_spine.coords` will use the raw, outlier-prone vertebra coordinates in the dataset. The constraint plane is a tuple of the vector pointing from one extreme of the spine to another and its origin shift\n",
    "\n",
    "The method `Spine.fit_longitudinal_plane()` returns the normal vector of the the longitudinal plane. For plotting, we set an origin shift for the normal vector such that the vector lies halfway between the spine extremes.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define the spine point cloud to use\n",
    "example_coords = utils.get_curve_coords(curves)\n",
    "\n",
    "# Define the constraint plane\n",
    "constraint_plane = utils.get_curve_extremes_vectors(curves)\n",
    "\n",
    "normal_vector = example_spine.fit_longitudinal_plane(\n",
    "    coords=example_coords, constraint_plane=constraint_plane\n",
    ")\n",
    "origin_shift = constraint_plane[1] + 0.5 * constraint_plane[0]\n",
    "print(\"Normal vector: {}\".format(normal_vector))\n",
    "print(\"Origin shift: {}\".format(origin_shift))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can directly overlay a plot of the longitudinal cut to the vertebra point cloud. The longitudinal plane is implicitly computed by the `Spine.plot_longitudinal_cut()` method and is represented by a transparent rectangle on the center of which lies the normal vector.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ax = plot.plot_spine(\n",
    "    example_spine,\n",
    "    curves=curves,\n",
    "    colors=example_spine.coords[\"vertebra\"],\n",
    "    origin=origin_shift,\n",
    ")\n",
    "ax = plot.plot_longitudinal_cut(\n",
    "    example_spine, coords=example_coords, ax=ax, constraint_plane=constraint_plane\n",
    ")\n",
    "ax.get_legend().remove()\n",
    "plt.tight_layout()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For an interactive version of the plot above, we can turn the backend of the spine plotting methods to `'plotly'`. The syntax changes slightly because now the returned objects are `plotly` figures and not `matplotlib` axes.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = plot.plot_spine(\n",
    "    example_spine,\n",
    "    curves=curves,\n",
    "    colors=example_spine.coords[\"vertebra\"],\n",
    "    origin=origin_shift,\n",
    "    backend=\"plotly\",\n",
    ")\n",
    "fig.add_trace(\n",
    "    plot.plot_longitudinal_cut(\n",
    "        example_spine,\n",
    "        coords=example_coords,\n",
    "        constraint_plane=constraint_plane,\n",
    "        backend=\"plotly\",\n",
    "    ).data[0]\n",
    ")\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Scoliosis distances\n",
    "\n",
    "We define the scoliosis distance of a point in the vertebral column as the distance of this point to the spine's longitudinal plane. The collection of scoliosis distances in a spine point cloud can be interpreted as samples from the scoliosis distribution of the spine.\n",
    "\n",
    "The list of scoliosis distances of the example spine can be obtained via the `.get_scoliosis_distances()` method. The first positional parameter tells the function which curve to use to find the longitudinal cut. The optional parameter `coords` indicates the set of coordinates to query for scoliosis distances.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Set and traversal sample space\n",
    "s = np.linspace(0, 1, 1000)\n",
    "\n",
    "# Get samples of curve coordinates\n",
    "curve_coords = utils.get_curve_coords(curves, s=s)\n",
    "\n",
    "# Get list of scoliosis distances per sample\n",
    "scoliosis_distances = example_spine.get_scoliosis_distances(coords=curve_coords)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### As we traverse the spine\n",
    "\n",
    "Since we gathered scoliosis distances for monotonically increasing traversal values `s`, we can plot the former versus the latter to see how the scoliosis distances change as we traverse the example spine from beginning to end. By construction, the scoliosis distances at the two spine extremities are zero. The distances then increase until reaching a maximum at around a third of the spine length, and then proceed to decrease again.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df = pd.DataFrame(\n",
    "    {\"Traversal parameter\": s, \"Scoliosis distance (mm)\": scoliosis_distances}\n",
    ")\n",
    "fig = px.line(\n",
    "    df,\n",
    "    x=\"Traversal parameter\",\n",
    "    y=\"Scoliosis distance (mm)\",\n",
    "    title=\"Scoliosis distances as we traverse the spine\",\n",
    ")\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Comparing distributions and extremes\n",
    "\n",
    "For a different perspective, we may plot the cumulative histogram of the list of scoliosis distances for the example spine. Overlaid to the histogram is the (cumulative) distribution derived from an estimated log-density function (via `nb_utils.estimate_log_density()`). The dashed red line marks the maximum scoliosis distance.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Estimate a log density function for the distribution of scoliosis distances\n",
    "example_log_density = nb_utils.estimate_log_density(scoliosis_distances)\n",
    "\n",
    "ax = nb_plotting.plot_cumulative_distribution(\n",
    "    scoliosis_distances, log_density=example_log_density\n",
    ")\n",
    "\n",
    "# Mark the maximum as a vertical line\n",
    "max_scoliosis = np.max(scoliosis_distances)\n",
    "ax.axvline(\n",
    "    x=max_scoliosis,\n",
    "    color=palette.rot,\n",
    "    label=\"Max. scoliosis distance = {:.1f}\".format(max_scoliosis),\n",
    "    linewidth=3,\n",
    "    ls=\"--\",\n",
    ")\n",
    "\n",
    "ax.legend(fontsize=12)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.1"
  },
  "vscode": {
   "interpreter": {
    "hash": "dd854e12a6a0e7de6bed4f13c9025e8341a08bfd29d8845554d66694bcec92da"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
