{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Comparing scoliosis\n",
    "\n",
    "This notebooks looks at the differences in spine curvatures between either dHT or Exon36 mice and their wild-type (WT) littermates.\n",
    "\n",
    "The plane where the largest variations in curvature happen should correspond to a longitudinal cut of the mouse's spine. Vertebra deviations from this longitudinal plane are thought of here, in this notebook, as a proxy for scoliosis. By definition, those vertebra deviations should be smaller in magnitude than the variations observed within the longitudinal plane. We will check if scoliosis deviations, however small, differ sufficiently across the two group of mice.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "import warnings\n",
    "\n",
    "import matplotlib.pyplot as plt\n",
    "import nb_plotting\n",
    "import nb_utils\n",
    "import numpy as np\n",
    "import plotly.express as px\n",
    "from scipy.stats import permutation_test\n",
    "\n",
    "from spinecurvatures import dataset, plot\n",
    "\n",
    "%reload_ext autoreload\n",
    "%autoreload 2\n",
    "\n",
    "plt.rcParams[\"font.family\"] = \"serif\"\n",
    "plt.rcParams[\"font.serif\"] = [\"Times New Roman\"]\n",
    "\n",
    "# CHOOSE HERE WHICH SUB-DATASET TO ANALYZE\n",
    "dataset_acronym = \"dHT\"\n",
    "# dataset_acronym = \"Exon36\"\n",
    "\n",
    "# Set data path\n",
    "SPINE_CURVATURES_DATA = os.environ.get(\"SPINE_CURVATURES_DATA\")\n",
    "DATA_PATH = os.path.join(\n",
    "    SPINE_CURVATURES_DATA, \"microtomography_data_{}_mice.xlsx\".format(dataset_acronym)\n",
    ")\n",
    "# Create directory for saving figures\n",
    "FIGURES_DIR = os.path.join(SPINE_CURVATURES_DATA, \"figures\")\n",
    "os.makedirs(FIGURES_DIR, exist_ok=True)\n",
    "\n",
    "# Create directory for saving results\n",
    "RESULTS_DIR = os.path.join(SPINE_CURVATURES_DATA, \"results\")\n",
    "os.makedirs(RESULTS_DIR, exist_ok=True)\n",
    "\n",
    "palette = plot.UnibasColors()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Load data\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "spine_dataset = dataset.SpineDataset(path=DATA_PATH)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Find the longitudinal cuts\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The longitudinal cuts of the spines of the whole dataset can be viewed all at once in a parallel plot. The first row contains the spines from the WT group, and the second row the ones from the transgenic group. Each spine is identified by the ID of the mouse it belongs to.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "curvature_info = nb_utils.compute_curvature_summaries(spine_dataset)\n",
    "fig = nb_plotting.plot_all_spines(\n",
    "    spine_dataset, curvature_info, with_longitudinal_cuts=True\n",
    ")\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Compute scoliosis deviations\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us overlap the scoliosis distance curves for all spines in the dataset. We color the curves according to the mouse's group to see if any patterns stand out across groups.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "scoliosis_df = nb_utils.compute_scoliosis_distances(\n",
    "    spine_dataset, curvature_info=curvature_info\n",
    ")\n",
    "\n",
    "with warnings.catch_warnings():\n",
    "    warnings.simplefilter(\"ignore\")\n",
    "    fig = px.scatter(\n",
    "        scoliosis_df,\n",
    "        x=\"Traversal parameter\",\n",
    "        y=\"Scoliosis distance (mm)\",\n",
    "        title=\"Scoliosis distances as we traverse the spine\",\n",
    "        color=\"Group\",\n",
    "        marginal_y=\"box\",\n",
    "    )\n",
    "\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Compare distributions and extremes\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us draw scoliosis distance distribution plots in parallel for all the mice in the dataset, comparing how their shape and the maximum scoliosis distances vary between groups. Under the hypothesis that the transgenic mice should have more intense scoliosis than the control mice, we would expect both the red lines to be consistently more to the right and the distributions to have a less sharp rise in the second row\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "scoliosis_info = nb_utils.compute_scoliosis_summaries(scoliosis_df)\n",
    "fig = nb_plotting.plot_scoliosis_summaries(scoliosis_info, xrange=(0, 2))\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Testing for differences\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "group_keys = scoliosis_df[\"Group\"].unique().tolist()\n",
    "summaries_group_1 = [\n",
    "    d[\"Maximum scoliosis\"] for d in scoliosis_info[group_keys[0]].values()\n",
    "]\n",
    "summaries_group_2 = [\n",
    "    d[\"Maximum scoliosis\"] for d in scoliosis_info[group_keys[1]].values()\n",
    "]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def statistic(sample_1, sample_2, axis=0):\n",
    "    return np.mean(sample_1, axis=axis) - np.mean(sample_2, axis=axis)\n",
    "\n",
    "\n",
    "res = permutation_test(\n",
    "    (summaries_group_1, summaries_group_2),\n",
    "    statistic,\n",
    "    vectorized=True,\n",
    "    n_resamples=np.inf,\n",
    "    alternative=\"less\",\n",
    ")\n",
    "\n",
    "print(\"Statistic for the original measurements = {:.2f}\".format(res.statistic))\n",
    "print(\"p-value = {:.5f}\".format(res.pvalue))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A plot of the permutation distribution of the test statistic confirms the initial suspicion: the difference in extreme scoliosis distances across the two groups of mice is not very important. Indeed, in almost 29% of all permutations we see a test statistic as small as the one in the original setting.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ax = nb_plotting.plot_permutation_distribution(res)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.1"
  },
  "vscode": {
   "interpreter": {
    "hash": "dd854e12a6a0e7de6bed4f13c9025e8341a08bfd29d8845554d66694bcec92da"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
