import pandas as pd

from spinecurvatures import models


class SpineDataset:
    """Dataset of mouse spines

    Parameters
    -----------
    path : str
        Path to the `.xlsx` file containing the grouping and vertebra
        coordinate information for the mouse spines
    imaging_resolution : float, optional
        Resolution of the imaging system in mm/pixel, by default 20e-3
    """

    def __init__(self, path, imaging_resolution=20e-3) -> None:
        self.path = path
        self.imaging_resolution = imaging_resolution
        self.groups, self.spines = self._gather_spines()

    def _clean_up_coords(self, coords):
        # Make sure to retain only the columns we need, in case there are more
        column_names = ["vertebra", "X", "Y", "Z"]
        coords = coords[column_names]

        # Change the dtype of 'X', 'Y', and 'Z' columns to float and map them to
        # physical units
        coords["X"] = coords["X"].astype(float) * self.imaging_resolution
        coords["Y"] = coords["Y"].astype(float) * self.imaging_resolution
        coords["Z"] = coords["Z"].astype(float) * self.imaging_resolution

        return coords

    def _gather_spines(self):
        # Reading the Excel file returns a dictionary of data frames with a key
        # per sheet
        dfs = pd.read_excel(self.path, sheet_name=None)

        # The first sheet has grouping information
        groups = dfs.pop("Groups")

        # Each of the other sheets is named by the mouse ID and contains the
        # coordinates of vertebra points in the abstract scan space
        spines = []
        for mouse_id, coords in dfs.items():
            spines.append(
                models.Spine(self._clean_up_coords(coords), mouse_id=int(mouse_id))
            )

        return groups, spines

    def get_spines_from_group(self, group):
        """Get spines from specified group

        Parameters
        ----------
        group : str
            Group string. See the column names of attribute `self.groups` for
            options

        Returns
        -------
        list
            List of Spine objects corresponding to vertebral columns in the
            specified group
        """
        spine_subset = []
        for spine in self.spines:
            if spine.mouse_id in list(self.groups[group]):
                spine_subset.append(spine)
        return spine_subset

    def get_spines_from_id(self, ids):
        """Get spines from specified mouse ID

        Parameters
        ----------
        ids : int or list of int
            Mouse ID(s)

        Returns
        -------
        Spine or list
            Spine object or list of Spine objects corresponding to vertebral
            columns with the specified mouse IDs
        """
        try:
            _ = len(ids)
        except TypeError:
            ids = [ids]

        spine_subset = []
        for spine in self.spines:
            if spine.mouse_id in ids:
                spine_subset.append(spine)

        if len(spine_subset) == 1:
            return spine_subset[0]
        else:
            return spine_subset

    def id2group(self, mouse_id):
        """Get group to which a mouse belongs

        Parameters
        ----------
        mouse_id : int
            Number identifying the queried mouse

        Returns
        -------
        str
            Name of the group to which the queried mouse belongs
        """
        for group in self.groups.columns:
            if mouse_id in list(self.groups[group]):
                return group
