import matplotlib.pyplot as plt
import mpl_toolkits.mplot3d as a3
import numpy as np
import plotly.express as px
import plotly.graph_objects as go

from spinecurvatures import utils


class UnibasColors:
    def __init__(self) -> None:
        self.mint = "#a5D7D2"
        self.mint_hell = "#d2ebe9"
        self.rot = "#d20537"
        self.anthrazit = "#2d373c"
        self.anthrazit_hell = "#46505a"


def plot_spine_slice(coord_slice, spine, curves=None, ax=None):
    """Plot X, Y, or Z slices of the spine in 2D

    Parameters
    ----------
    coord_slice : str
        Coordinate slice to plot. Options are `'X'`, `'Y'`, and `'Z'`
    spine : `spinecurvatures.dataset.Spine`
        A Spine object
    curves : tuple, optional
        X, Y, and Z coordinates of a curve parametrized by a scalar parameter
    ax : `matplotlib.axes.Axes`, optional
        Axes onto which make the plot, by default None, which means a new
        axis will be created

    Returns
    -------
    `matplotlib.axes.Axes`
        A matplotlib Axis object
    """
    # Colors of the scattered vertebra points
    palette = UnibasColors()

    # Parse slicing
    if coord_slice == "X":
        points = spine.coords["X"]
        curve_id = 0
    elif coord_slice == "Y":
        points = spine.coords["Y"]
        curve_id = 1
    elif coord_slice == "Z":
        points = spine.coords["Z"]
        curve_id = 2
    else:
        raise ValueError(
            "Invalid value for `slice` parameter."
            + " Options are `None`, `'X'`, `'Y'` or `'Z'`."
        )

    # Scatter 1D-measurements for the vertebra locations
    if ax is None:
        fig = plt.figure(figsize=(9, 9))
        ax = fig.add_subplot(111)
    ax.scatter(
        np.linspace(0, 1, len(points)),
        points,
        label="Measurements",
        c=palette.anthrazit_hell,
    )
    ax.set(xlabel="Traversal parameter", ylabel=f"{coord_slice}-coordinate (mm)")

    # Overlay smooth fitted curve
    if curves is not None:
        s_vals = np.linspace(0, 1, 10 * len(spine.coords))
        ax.plot(
            s_vals,
            curves[curve_id](s_vals),
            c=palette.rot,
            linewidth=2,
            label="Smooth curve fit",
        )

    ax.legend(fontsize=14)
    return ax


def _plot_spine_matplotlib(
    coords, curves=None, s_vals=None, origin=np.zeros((3,)), ax=None
):
    if ax is None:
        fig = plt.figure(figsize=(6, 6))
        ax = fig.add_subplot(111, projection="3d")

    # Scatter X,Y,Z-measurements for the vertebra locations
    # (origin-centered)
    palette = UnibasColors()
    ax.scatter(
        coords["X"] - origin[0],
        coords["Y"] - origin[1],
        coords["Z"] - origin[2],
        label="Measurements",
        c=palette.anthrazit_hell,
    )

    # Set aspect ratio of the three axes to follow the same ranges of
    # variation as the x,y,z-coordinates
    ax.set_box_aspect((np.ptp(coords["X"]), np.ptp(coords["Y"]), np.ptp(coords["Z"])))

    # Overlay smooth fitted curve
    if curves is not None:
        ax.plot(
            curves[0](s_vals) - origin[0],
            curves[1](s_vals) - origin[1],
            curves[2](s_vals) - origin[2],
            c=palette.rot,
            linewidth=2,
            label="Smooth curve fit",
        )

    # Remove axis ticks for a cleaner plot
    ax.set_xticks([])
    ax.set_yticks([])
    ax.set_zticks([])

    # Turn on legend
    ax.legend(fontsize=14)

    return ax


def _plot_spine_plotly(
    coords, curves=None, s_vals=None, origin=np.zeros((3,)), colors=None
):
    # Center the coordinates
    coords.loc[:, "X"] -= origin[0]
    coords.loc[:, "Y"] -= origin[1]
    coords.loc[:, "Z"] -= origin[2]

    # Scatter the coordinates
    fig = px.scatter_3d(
        coords,
        x="X",
        y="Y",
        z="Z",
        hover_data={"X": False, "Y": False, "Z": False, "vertebra": True},
    )

    fig.update_traces(marker=dict(size=4, opacity=0.7))

    if colors is not None:
        fig.update_traces(marker=dict(color=colors))

    if curves is not None:
        curve_coords = utils.get_curve_coords(curves, s=s_vals)

        # Center the coordinates
        curve_coords.loc[:, "X"] -= origin[0]
        curve_coords.loc[:, "Y"] -= origin[1]
        curve_coords.loc[:, "Z"] -= origin[2]
        curve_coords["name"] = "Smooth curve fit"

        fig.add_trace(
            px.line_3d(curve_coords, x="X", y="Y", z="Z", hover_name="name").data[0],
        )

        palette = UnibasColors()
        fig.update_traces(line=dict(color=palette.anthrazit, width=8))

    fig.update_layout(
        scene=dict(
            aspectmode="data",
            xaxis=dict(showticklabels=False),
            yaxis=dict(showticklabels=False),
            zaxis=dict(showticklabels=False),
            camera=dict(
                eye=dict(x=2, y=2, z=2),
            ),
        ),
        margin=dict(l=0, r=0, b=0, t=0, pad=0),
        width=420,
        height=720,
    )

    return fig


def plot_spine(
    spine,
    curves=None,
    s_vals=None,
    coords=None,
    origin=np.zeros((3,)),
    colors=None,
    backend="matplotlib",
    ax=None,
):
    """Plot vertebra measurements,

    Vertebra measurements can be potentially overlaid with a smooth fitted
    curve

    Parameters
    ----------
    spine : `spinecurvatures.dataset.Spine`
        A Spine object
    curves : tuple, optional
        X, Y, and Z coordinates of a curve parametrized by a scalar parameter
    s_vals : list, optional
        List of traversal parameters to use when plotting the curve, by
        default `np.linspace(0, 1, 10*len(coords))`
    coords : `pandas.DataFrame`, optional
        Set of vertebra coordinates to use when scatter plotting, by
        default the coordinates stored in `self.coords` will be used
    colors : optional
        List of values to use as colors for the scatter plot. Should be the same length as coords. If None, a default color palette is used
    origin : array-like, optional
        Vector denoting where the origin of the coordinate system should be, by
        default `numpy.zeros((3,))`
    backend : str, optional
        Plotting backend to use, by default `'matplotlib'`. Other options are
        `'plotly'`
    ax : `matplotlib.axes.Axes`, optional
        Axes onto which make the plot, by default None, which means a new
        axis will be created. Only used if `backend=='matplotlib'

    Returns
    -------
    `matplotlib.axes.Axes`
        A matplotlib Axis object if `backend=='matplotlib'`

    `plotly.graph_objects.Figure`
        A plotly Figure object if `backend=='plotly'`

    Raises
    ------
    ValueError
        If `backend` is neither `'matplotlib'` nor `'plotly'`
    ValueError
        If `slice` is not `None`, `'X'`, `'Y'`, or `'Z'`
    """
    # Pick the set of coordinates to use
    coords = spine.coords if coords is None else coords
    coords = coords.copy(deep=True)

    s_vals = np.linspace(0, 1, 10 * len(coords)) if s_vals is None else s_vals

    if backend == "matplotlib":
        return _plot_spine_matplotlib(
            coords, curves=curves, s_vals=s_vals, origin=origin, ax=ax
        )

    elif backend == "plotly":
        return _plot_spine_plotly(
            coords, curves=curves, s_vals=s_vals, origin=origin, colors=colors
        )

    else:
        raise ValueError("Backend options are `matplotlib` and `plotly`")


def _plot_longitudinal_plane_matplotlib(extremities, origin, direction, ax=None):
    # Parse axis
    if ax is None:
        fig = plt.figure(figsize=(6, 6))
        ax = fig.add_subplot(111, projection="3d")

    palette = UnibasColors()

    # Draw a section of the longitudinal plane
    rect = a3.art3d.Poly3DCollection(
        [extremities],
        color=palette.anthrazit_hell,
        alpha=0.4,
        linewidth=1,
        linestyle="--",
    )
    ax.add_collection3d(rect)

    # Draw the normal vector
    ax.quiver(
        origin[0],
        origin[1],
        origin[2],  # <-- starting point
        direction[0],
        direction[1],
        direction[2],  # <-- direction
        color=palette.anthrazit,
        alpha=1,
        lw=2.5,
    )

    return ax


def _plot_longitudinal_plane_plotly(extremities):
    # Stack first rectangle extreme vector to the extremities array so that
    # plotly closes the polygon
    extremities = np.vstack([extremities, extremities[0, :]])

    # Draw the longitudinal cut rectangle as a filled-in scatter plot
    palette = UnibasColors()
    fig = go.Figure(
        go.Scatter3d(
            x=extremities[:, 0],
            y=extremities[:, 1],
            z=extremities[:, 2],
            name="Longitudinal cut",
            surfaceaxis=0,
            surfacecolor=palette.anthrazit_hell,
            opacity=0.4,
            marker=dict(
                size=1, opacity=0.7, color=palette.anthrazit_hell, showscale=False
            ),
            showlegend=False,
            hoverinfo="skip",
        )
    )

    return fig


def plot_longitudinal_cut(
    spine, coords=None, origin=np.zeros((3,)), backend="matplotlib", ax=None, **kwargs
):
    """Plot longitudinal cut (and normal vector) of a spine

    Parameters
    ----------
    spine : `spinecurvatures.dataset.Spine`
        A spine object
    coords : `pandas.DataFrame`, optional
            Set of coordinates to use when fitting the longitudinal plane, by
            default the coordinates stored in `spine.coords` will be used
    origin : array-like, optional
        Vector denoting where the origin of the coordinate system should be, by
        default `numpy.zeros((3,))`
    backend : str, optional
        Plotting backend to use, by default `'matplotlib'`. Other options are
        `'plotly'`
    ax : `matplotlib.axes.Axes`, optional
        The axis onto which to plot the longitudinal cut, by default a new
        figure/axis is created. Only used if `backend=='matplotlib'
    **kwargs : dict, optional
        Extra keyword arguments for
        `spinecurvatures.dataset.Spine.fit_longitudinal_plane`

    Returns
    -------
    `matplotlib.axes.Axes`
        A matplotlib Axis object if `backend=='matplotlib'`

    `plotly.graph_objects.Figure`
        A plotly Figure object if `backend=='plotly'`

    Raises
    ------
    ValueError
        If `backend` is neither `'matplotlib'` nor `'plotly'`
    """
    # Pick the set of coordinates to use
    coords = spine.coords if coords is None else coords

    # Get vectors spanning the longitudinal plane and the normal vector
    kwargs.pop("return_all", None)
    normal_vector, plane_vector_2, plane_vector_1 = spine.fit_longitudinal_plane(
        coords=coords, return_all=True, **kwargs
    )

    # Define scaling factors
    cube_diag = np.array(
        [np.ptp(coords["X"]), np.ptp(coords["Y"]), np.ptp(coords["Z"])]
    )
    plane_scale_factor = np.linalg.norm(cube_diag) / 3
    vector_scale_factor = np.linalg.norm(cube_diag) / 9

    # Define extreme points of the rectangle using the spanning vectors
    extreme_1 = plane_scale_factor * (plane_vector_1 + plane_vector_2)
    extreme_2 = plane_scale_factor * (-plane_vector_1 + plane_vector_2)
    extreme_3 = plane_scale_factor * (-plane_vector_1 - plane_vector_2)
    extreme_4 = plane_scale_factor * (plane_vector_1 - plane_vector_2)
    extremities = np.array([extreme_1, extreme_2, extreme_3, extreme_4])

    # Shift extremes to the desired origin
    extremities += origin

    # Scale the plane's normal vector
    direction = (vector_scale_factor * normal_vector) + origin

    if backend == "matplotlib":
        return _plot_longitudinal_plane_matplotlib(
            extremities, origin, direction, ax=ax
        )
    elif backend == "plotly":
        return _plot_longitudinal_plane_plotly(extremities)
    else:
        raise ValueError("Backend options are `matplotlib` and `pyplot`")
