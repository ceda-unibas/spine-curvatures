import numpy as np
import pandas as pd
from scipy.interpolate import BSpline, splev
from scipy.optimize import minimize, minimize_scalar
from sklearn.decomposition import PCA

from spinecurvatures import utils


def wlsq(*args):
    """(Weighted) least-squares error of a spline approximation

    Parameters
    ----------
    *args : tuple
        6 arguments to pass to the least-squares error function. Should
        be, in order:
        - c : array_like, Spline coefficients
        - x : array_like, Abscissas
        - y : array_like, Ordinates
        - t : array_like, List of spline knots
        - k : int, Spline degree
        - w : array_like, optional, weights to apply when computing least squares. Must be the same shape as `y`. If None, then all weights are one

    Returns
    -------
    float
        (Weighted) least-squares error
    """
    # Parse arguments
    c, x, y, t, k, w = args

    # Compute (weighted) least-squares error between the spline and the data
    diff = y - splev(x, BSpline(t, c, k))
    if w is None:
        diff = np.sum(diff * diff)
    else:
        diff = np.dot(diff * diff, w)

    return np.abs(diff)


def fit_constrained_spline(x, y, w=None):
    """Constrained least-squares spline fit

    Fit a spline to data with the constraint that the spline has zero second-order
    derivatives at the left and right extremes of the curve, so that the ends of the
    spine that will be fitted are straight. Good model for fitting the mouse spine.

    Parameters
    ----------
    x : array_like
        Abscissas
    y : array_like
        Ordinates
    w : array_like, optional
        Weights to apply when computing least squares. Must be the same shape
        as `y`. If None, then all weights are one

    Returns
    -------
    `scipy.interpolate.BSpline`
        A spline object that fits the data
    """
    # Set the spline degree
    k = 3

    # Set up the internal knots: 4, equally spaced
    internal_knots = [0.225, 0.5, 0.725, 0.95]

    # Make the knot vector (k+1)-regular by adding boundary knots
    t = np.r_[(x[0],) * (k + 1), internal_knots, (x[-1],) * (k + 1)]

    # Set initial coefficients
    c0 = np.zeros((len(t),))
    c0[: k + 1] = 1.0

    # Set constraints: the fitted spline is forced to have zero second-order
    # derivatives at the left and right extremes of the curve, so that the ends of the
    # spine that will be fitted are straight
    constraints = [
        {"type": "eq", "fun": lambda c: splev(x[0], BSpline(t, c, k), der=2)},
        {"type": "eq", "fun": lambda c: splev(x[-1], BSpline(t, c, k), der=2)},
    ]

    # Optimize
    opt = minimize(wlsq, c0, args=(x, y, t, k, w), constraints=constraints)
    c_opt = opt.x
    return BSpline(t, c_opt, k)


def parse_bounds(bounds: tuple = None) -> tuple:
    """Parse bounds and set default values if necessary

    Parameters
    ----------
    bounds : tuple, optional
        Bounds on the curve traversal parameter, by default None

    Returns
    -------
    tuple
        Bounds on the traversal parameter. If `bounds` is None, then the
        default bounds are (0.0, 1.0)
    """
    bounds = (0.0, 1.0) if bounds is None else bounds
    return bounds


class Spine:
    """Spine model

    Parameters
    ----------
    coords : `pandas.DataFrame`
        A table containing at least three columns : `'X'`, `'Y'`, `'Z'`,
        representing three-dimensional coordinates of vertebra points in
        the spine.
    mouse_id : str, optional
        Unique identifier for the mouse to whom this spine belongs to, by
        default None
    fit_curves_on_build : bool, optional
        Fit smooth curves to the spine when building the object, by default
        True

    Attributes
    ----------
    coords : `pandas.DataFrame`
        A table containing at least three columns : `'X'`, `'Y'`, `'Z'`,
        representing three-dimensional coordinates of vertebra points in
        the spine.
    mouse_id : str
        Unique identifier for the mouse to whom this spine belongs to
    segment_1 : tuple
        Lower and upper bounds of the first segment of interest in the
        vertebral column
    segment_2 : tuple
        Lower and upper bounds of the second segment of interest in the
        vertebral column
    curves : tuple
        A tuple of `scipy.interpolate.BSpline` objects representing the
        smooth curves fitted to the spine. Will raise an error if no curves have
        been fitted yet
    velocities : tuple
        A tuple of `scipy.interpolate.BSpline` objects representing the
        velocity functions of the parametrized curves. Will raise an error if no
        velocity functions have been computed yet
    accelerations : tuple
        A tuple of `scipy.interpolate.BSpline` objects representing the
        acceleration functions of the parametrized curves. Will raise an error if
        no acceleration functions have been computed yet

    Methods
    -------
    fit_curves()
        Fit smooth splines to vertebra coordinates
    find_nearest_vertebra(s)
        Find the nearest vertebra to a point on a smooth curve
    find_segment_bounds(segment)
        Find traversal parameter bounds that traverse a spine segment
    get_coords_by_vertebra(vertebras)
        Filter the coordinate table by a list of vertebras
    get_velocities()
        Shortcut for getting velocity functions from parametrized curves
    get_accelerations()
        Shortcut for getting acceleration functions from parametrized curves
    curvature(s)
        Pointwise curvature of the curves parametrized by a scalar
    radius_of_curvature(s)
        Pointwise radius of curvature
    max_curvature(bounds=None, **kwargs)
        Find maximum curvature of a parametrized curve and corresponding parameter
    avg_curvature(bounds=None, n_points=1e4, **kwargs)
        Compute average curvature of a parametrized curve
    fit_longitudinal_plane(coords=None, constraint_plane=None, return_all=False)
        Fit plane that cuts the spine longitudinally
    get_centroid(coords=None)
        Get centroid of a point cloud
    get_scoliosis_distances(coords=None)
        Get list of scoliosis distances
    estimate_length(n_points=100)
        Estimate the length of the spine
    """

    def __init__(self, coords, mouse_id=None, fit_curves_on_build=True) -> None:
        self.coords = coords
        self.mouse_id = mouse_id

        # Segments of interest in the vertebral column. The segments are
        # represented by lower and upper bounds of intervals of vertebra
        # numbers
        self.segment_1 = (1, 13)
        self.segment_2 = (13, 23)

        # Fit smooth curves to the spine
        self._init_curve_attributes()
        if fit_curves_on_build:
            self.fit_curves()
            self.get_velocities()
            self.get_accelerations()

    def _init_curve_attributes(self):
        self._curves = None
        self._velocities = None
        self._accelerations = None

    @property
    def curves(self):
        if self._curves is None:
            raise ValueError(
                "No smooth curves have been fitted to the spine yet. Call the "
                "`fit_curves` method to fit smooth curves to the spine."
            )
        return self._curves

    @property
    def velocities(self):
        if self._velocities is None:
            raise ValueError(
                "No velocity functions have been computed for the spine yet. Call the "
                "`get_velocities` method to compute velocity functions."
            )
        return self._velocities

    @property
    def accelerations(self):
        if self._accelerations is None:
            raise ValueError(
                "No acceleration functions have been computed for the spine yet. Call the "
                "`get_accelerations` method to compute acceleration functions."
            )
        return self._accelerations

    def fit_curves(self):
        """Fit smooth splines to vertebra coordinates

        Returns
        -------
        tuple
            A `scipy.interpolate.BSpline` object for each of the X,
            Y, and Z axes (in order).
        """
        # Get X-, Y-, and Z-coordinates of the vertebras. Deep copy them
        # because we will modify them in case they have NaN values
        x = self.coords["X"].copy(deep=True)
        y = self.coords["Y"].copy(deep=True)
        z = self.coords["Z"].copy(deep=True)

        # Deal with potential NaN coordinates by setting corresponding
        # multiplicative weights to zero. This is a recommended trick from the
        # scipy documentation
        w_x = np.isnan(x)
        x[w_x] = 0.0
        w_y = np.isnan(y)
        y[w_y] = 0.0
        w_z = np.isnan(z)
        z[w_z] = 0.0

        # Set the traversal parameter for the fitted curves
        s = np.linspace(0, 1, len(x))

        # Fit a constrained smoothed spline (least-squares minimization) to
        # each of the three coordinates.
        curve_x = fit_constrained_spline(s, x, w=~w_x)
        curve_y = fit_constrained_spline(s, y, w=~w_y)
        curve_z = fit_constrained_spline(s, z, w=~w_z)

        self._curves = (curve_x, curve_y, curve_z)

    def find_nearest_vertebra(self, s):
        """Find the nearest vertebra to a point on a smooth curve

        Parameters
        ----------
        s : float
            Traversal parameter. Must be between 0.0 and 1.0 for valid results

        Returns
        -------
        int
            The vertebra number closest to the point on the curve at traversal
            parameter `s`
        """
        # Represent vertebras by their average coordinate
        avg_vertebra_coords = self.coords.groupby("vertebra").mean()

        # Get coordinates of the point at traversal parameter s
        curves = self.curves
        point = np.array([curves[0](s), curves[1](s), curves[2](s)])

        # Loop over vertebras to find the nearest one
        min_dist = np.inf
        nearest_vertebra = None
        for vertebra in avg_vertebra_coords.index:
            vertebra_coord = avg_vertebra_coords.loc[vertebra]
            dist = np.linalg.norm(point - vertebra_coord)
            if dist < min_dist:
                min_dist = dist
                nearest_vertebra = vertebra

        return nearest_vertebra

    def _binary_search_traversal_parameter(
        self, target_vertebra, bounds, from_below=True
    ):
        while bounds[0] < bounds[1]:
            mid = (bounds[0] + bounds[1]) / 2
            mid_vertebra = self.find_nearest_vertebra(mid)
            if mid_vertebra < target_vertebra:
                bounds = (mid, bounds[1])
            elif mid_vertebra > target_vertebra:
                bounds = (bounds[0], mid)
            else:
                bounds = (mid, mid)

        return (bounds[0] + bounds[1]) / 2

    def find_segment_bounds(self, segment):
        """Find traversal parameter bounds that traverse a spine segment

        Parameters
        ----------
        segment : tuple
            Two vertebra numbers indicating the start and end of the segment in
            the spine

        Returns
        -------
        tuple
            Lower and upper bounds on the traversal parameter required to
            traverse the specified segment
        """
        lower_bound = self._binary_search_traversal_parameter(
            segment[0],
            (0.0, 1.0),
        )
        upper_bound = self._binary_search_traversal_parameter(
            segment[1],
            (lower_bound, 1.0),
        )
        return lower_bound, upper_bound

    def get_coords_by_vertebra(self, vertebras):
        """Filter the coordinate table by a list of vertebras

        Parameters
        ----------
        vertebras : list
            Indices of the vertebras for which to return the coordinates

        Returns
        -------
        `pandas.DataFrame`
            A table of vertebra coordinates in the format of `self.coordinates`
            but containing coordinates only for the vertebras in the provided
            list
        """
        # Build a mask that will indicate the subset of the `self.coords` data
        # frame in where the `vertebra` column has a value equal to some number
        # in the `vertebras` list
        mask = pd.DataFrame(self.coords.vertebra.tolist()).isin(vertebras).any(1).values

        # Filter `self.coords` by the mask to get the desired slice
        coords = self.coords[mask]

        return coords

    def _get_derivatives(self, curves, order):
        """Get derivatives vector of provided order for the parametrized curves

        Parameters
        ----------
        curves : tuple
            A tuple representing parametrized curves along the X, Y,
            and Z axes. Each element of the tuple should implement a method `.
            derivative` for computing the elementwise derivatives of the smooth
            curves.
        order : int
            Order of the derivative to compute

        Returns
        -------
        tuple
            Derivative along the X, Y, and Z axes as a function of the traversal parameter
        """
        derivatives = (
            curves[0].derivative(order),
            curves[1].derivative(order),
            curves[2].derivative(order),
        )
        return derivatives

    def get_velocities(self):
        """Shortcut for getting velocity functions from parametrized curves"""
        self._velocities = self._get_derivatives(self.curves, 1)

    def get_accelerations(self):
        """Shortcut for getting acceleration functions from parametrized curves"""
        self._accelerations = self._get_derivatives(self.curves, 2)

    def curvature(self, s):
        """Pointwise curvature of the curves parametrized by a scalar

        Parameters
        ----------
        s : float
            Traversal parameter. Must be between 0.0 and 1.0 for valid results

        Returns
        -------
        float
            The curvature of the 3D curve at point s

        See also
        --------
        `spinecurvatures.dataset.Spine.fit_curves`: Fit smooth splines to
            vertebra coordinates
        """
        # Get velocity and acceleration functions
        vel_x, vel_y, vel_z = self.velocities
        accel_x, accel_y, accel_z = self.accelerations

        # Compute pointwise curvature according to formula for general scalar
        # parametrizations
        vel_s = np.array([vel_x(s), vel_y(s), vel_z(s)])
        accel_s = np.array([accel_x(s), accel_y(s), accel_z(s)])
        numerator = np.linalg.norm(np.cross(vel_s, accel_s))
        denominator = np.linalg.norm(vel_s) ** 3
        kappa = numerator / denominator

        return kappa

    def radius_of_curvature(self, s):
        """Pointwise radius of curvature

        Parameters
        ----------
        s : float
            Traversal parameter. Must be between 0.0 and 1.0 for valid results

        Returns
        -------
        float
            The radius of curvature of the 3D curve at point s

        See also
        --------
        `spinecurvatures.dataset.Spine.fit_curves`: Fit smooth splines to
            vertebra coordinates
        """
        # Radius of curvature is the reciprocal of the curvature
        return 1 / self.curvature(s)

    def max_curvature(self, bounds=None, **kwargs):
        """Find maximum curvature of a parametrized curve and corresponding parameter

        Parameters
        ----------
        bounds : tuple, optional
            Lower and upper bounds on the traversal parameter to restrict the
            search space for the maximum, by default (0.0, 1.0)
        kwargs : dict, optional
            Extra keyword arguments for the method `self.curvature`

        Returns
        -------
        `scipy.optimize.OptimizeResult`
            Result of the optimization. The maximum curvature is stored in the
            `fun` attribute. The traversal parameter that maximizes the
            curvature is stored in the `x` attribute
        """
        bounds = parse_bounds(bounds)
        res = minimize_scalar(
            lambda s: -self.curvature(s, **kwargs), bounds=bounds, method="bounded"
        )
        res.fun = -res.fun
        return res

    def avg_curvature(self, bounds=None, n_points=1e4, **kwargs):
        """Compute average curvature of a parametrized curve

        Parameters
        ----------
        bounds : tuple, optional
            Lower and upper bounds on the traversal parameter to restrict the
            search space for the maximum, by default (0.0, 1.0)
        n_points : int, optional
            Number of sample points to take from the continuous curve in order
            to compute the average, by default 1e4
        kwargs : dict, optional
            Extra keyword arguments for the method `self.curvature`

        Returns
        -------
        float
            Average curvature
        """
        bounds = parse_bounds(bounds)
        s = np.linspace(bounds[0], bounds[1], int(n_points))
        avg = 0
        for val in s:
            avg += self.curvature(val, **kwargs)
        avg /= n_points
        return avg

    def fit_longitudinal_plane(
        self, coords=None, constraint_plane=None, return_all=False
    ):
        """Fit plane that cuts the spine longitudinally

        The longitudinal cut is defined as the plane spanned by the first two
        principal components of the vertebra coordinate point-cloud

        Parameters
        ----------
        coords : `pandas.DataFrame`, optional
            Set of coordinates to use when fitting the longitudinal plane, by
            default the coordinates stored in `self.coords` will be used
        constraint_plane : tuple, optional
            The normal vector and the origin shift defining an affine plane
            that will be used to constrain the longitudinal plane. If provided,
            the normal vector will belong to the longitudinal plane. Then, the
            spine points will be projected onto the constraint plane and PCA
            will reveal the other longitudinal plane vector and the vector
            normal to the longitudinal plane as the first and second principal
            components, respectively. By default, no constraint will be applied
        return_all : bool, optional
            Return normal vector as well as the two vectors spanning the
            longitudinal plane, by default False

        Returns
        -------
        array_like
            The normal vector defining the longitudinal plane, if
            `return_all==False`
        tuple
            In order, the normal vector defining the longitudinal plane, and
            the two orthogonal vectors spanning the longitudinal plane, if
            `return_all==True`
        """
        # Pick the set of coordinates to use
        coords = self.coords if coords is None else coords
        X = coords.loc[:, "X":"Z"].copy(deep=True).to_numpy()

        if constraint_plane is not None:
            longitudinal_vector_1 = np.copy(constraint_plane[0])
            origin_shift = np.copy(constraint_plane[1])
            # Project coordinates onto constraint plane
            X = utils.project_onto_affine_plane(X, longitudinal_vector_1, origin_shift)
            # Setup PCA with 2 components
            pca = PCA(n_components=2)
            pca.fit(X)
            longitudinal_vector_1 /= np.linalg.norm(longitudinal_vector_1)
            longitudinal_vector_2 = pca.components_[0, :]
        else:
            # Setup PCA with 3 components
            pca = PCA(n_components=3)
            pca.fit(X)
            longitudinal_vector_1 = pca.components_[0, :]
            longitudinal_vector_2 = pca.components_[1, :]

        # Fit PCA
        pca.fit(X)

        # Normal vector is obtained by the third PCA component
        normal_vector = pca.components_[-1, :]

        if return_all:
            return normal_vector, longitudinal_vector_2, longitudinal_vector_1
        else:
            return normal_vector

    def get_centroid(self, coords=None):
        """Get centroid of a point cloud

        Compute shift vector from the origin to the centroid of the coordinate
        point cloud

        Parameters
        ----------
        coords : `pandas.DataFrame`, optional
            Set of coordinates to use when computing the origin shift, by
            default the coordinates stored in `self.coords` will be used

        Returns
        -------
        `numpy` array
            x,y,z-coordinates of the centroid of the `coords` point cloud
        """
        # Pick the set of coordinates to use
        coords = self.coords if coords is None else coords

        x = coords["X"].mean()
        y = coords["Y"].mean()
        z = coords["Z"].mean()

        return np.array([x, y, z])

    def get_scoliosis_distances(self, coords=None):
        """Get list of scoliosis distances

        The scoliosis distance of a point is defined as its distance to the
        longitudinal plane of the spine

        Parameters
        ----------
        coords : `pandas.DataFrame`, optional
            Set of coordinates to query for scoliosis distances,
            by default the coordinates stored in `self.coords` will be used.

        Returns
        -------
        `numpy` array
            List of scoliosis distances. Follows the same indexing as
            `self.coords`
        """
        # Pick the sets of coordinates to use
        coords = self.coords if coords is None else coords

        # Get origin shift and vector normal to the longitudinal plane
        curves = self.curves
        constraint_plane = utils.get_curve_extremes_vectors(curves)
        curve_coords = utils.get_curve_coords(curves)
        normal_vector = self.fit_longitudinal_plane(
            coords=curve_coords, constraint_plane=constraint_plane
        )

        # Get origin shift
        origin_shift = constraint_plane[1]

        # Scoliosis distances are the distances of the vertebras to the plane
        # of the longitudinal cut
        X = coords.loc[:, "X":"Z"].to_numpy()
        X = np.subtract(X, origin_shift)
        scoliosis_distances = np.abs(np.inner(X, normal_vector))

        return scoliosis_distances

    def estimate_length(self, n_points=100):
        """Estimate the length of the spine

        Parameters
        ----------
        n_points : int, optional
            Number of uniformly spaced points along the spine to estimate the
            length, by default 100

        Returns
        -------
        float
            Estimated length of the spine
        """
        # Sample space of traversal parameters
        s = np.linspace(0, 1, n_points)

        # Get cumulative sum of segment lengths approximated by
        # straight lines
        curves = self.curves
        total_length = 0
        for i in range(n_points - 1):
            start_point = np.array([curves[0](s[i]), curves[1](s[i]), curves[2](s[i])])
            end_point = np.array(
                [curves[0](s[i + 1]), curves[1](s[i + 1]), curves[2](s[i + 1])]
            )
            segment_vector = end_point - start_point
            total_length += np.linalg.norm(segment_vector)

        return total_length
