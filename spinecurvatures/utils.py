import numpy as np
import pandas as pd


def get_curve_coords(curves, s=None):
    """Get 3D coordinates of points in a curve specified by a scalar parameter

    Parameters
    ----------
    curves : tuple
        A tuple of callables representing parametrized curves along the X, Y,
        and Z axes.
    s : array-like, optional
        List of traversal parameters to query for coordinates, by default
        `numpy.linspace(0, 1, 1000)`

    Returns
    -------
    `pandas.DataFrame`
        Data frame containing X,Y,Z-coordinates of the 3D curve at the queried
        traversal parameters
    """
    # Set traversal parameters
    s = np.linspace(0, 1, 1000) if s is None else s

    # Set curve coordinates as a data frame following the same conventions as
    # the one stored in `spinecurvatures.dataset.Spine` objects
    curve_coords = pd.DataFrame()
    curve_coords["X"] = curves[0](s)
    curve_coords["Y"] = curves[1](s)
    curve_coords["Z"] = curves[2](s)

    return curve_coords


def project_onto_affine_plane(points, normal_vector, origin_shift):
    """Project point onto affine plane

    Parameters
    ----------
    point : array_like
        Points to project
    normal_vector : array_like
        Normal vector of the affine plane
    origin_shift : array_like
        Origin shift of the affine plane

    Returns
    -------
    array_like
        Projected points
    """
    # Make sure normal vector is of unity norm
    normal_vector = normal_vector / np.linalg.norm(normal_vector)

    # Translate point by the origin shift
    translates = np.subtract(points, origin_shift)

    # Compute projection of translate onto the linear subspace spanned by the
    # normal vector
    magnitudes = np.inner(translates, normal_vector)
    try:
        # Do outer product of magnitude values and the normal vector
        proj_normal = np.einsum("i,j", magnitudes, normal_vector)
    except ValueError:
        # If `magnitudes` is a single float, we can simply multiply it to the
        # normal vector
        proj_normal = magnitudes * normal_vector

    # Remove the latter from the point to retrieve the projection onto the
    # affine plane
    proj = np.subtract(points, proj_normal)

    return proj


def get_curve_extremes_vectors(curves, s_1=0.0, s_2=1.0):
    """Get the vector pointing from the first extreme of the curve to second,
    and the vector pointing from the origin to the first extreme

    Parameters
    ----------
    curves : tuple
        A tuple of callables representing parametrized curves along the X, Y,
        and Z axes.
    s_1 : int, optional
        Traversal parameter defining the first extreme of the curve, by
        default 0
    s_2 : int, optional
        Traversal parameter defining the second extreme of the curve, by
        default 1

    Returns
    -------
    tuple
        In order: the vector pointing from the first extreme of the curve to
        second, and the vector pointing from the origin to the first extreme

    """
    point_1 = np.array([curves[0](s_1), curves[1](s_1), curves[2](s_1)])
    point_2 = np.array([curves[0](s_2), curves[1](s_2), curves[2](s_2)])

    return (point_2 - point_1), point_1
