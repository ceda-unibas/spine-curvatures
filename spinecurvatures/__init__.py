"""The :mod:`spinecurvatures` Python package"""

__version__ = "1.0.0"
__author__ = "Rodrigo C. G. Pena"
__credits__ = "Center for Data Analysis (CeDA) - University of Basel"
